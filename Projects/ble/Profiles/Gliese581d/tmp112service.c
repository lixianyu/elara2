/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "OSAL.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "gapbondmgr.h"
#include "gapgattserver.h"
#include "tmp112service.h"
#include "hal_adc.h"
/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

#define SERVAPP_NUM_ATTR_SUPPORTED        4

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
// Tmp112 GATT Profile Service UUID: 0c57270a-30a7-4d7a-a336-7cbe27df2a5b
CONST uint8 tmp112ProfileServUUID[ATT_UUID_SIZE] =
{
    TMP112PROFILE_SERV_UUID
};

// Characteristic 1 UUID: 11175c20-831e-4105-a036-f693136745f7
CONST uint8 tmp112Profilechar1UUID[ATT_UUID_SIZE] =
{
    TMP112PROFILE_CHAR1_UUID
};


/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static tmp112ProfileCBs_t *tmp112Profile_AppCBs = NULL;

/*********************************************************************
 * Profile Attributes - variables
 */

// Tmp112 Profile Service attribute
static CONST gattAttrType_t tmp112ProfileService = { ATT_UUID_SIZE, tmp112ProfileServUUID };


// Tmp112 Profile Characteristic 1 Properties
static uint8 tmp112ProfileChar1Props = GATT_PROP_WRITE | GATT_PROP_READ;
// Characteristic 1 Value -- Set major and minor.
static uint8 tmp112ProfileChar1[TMP112PROFILE_CHAR1_LEN] = {0};
// Tmp112 Profile Characteristic 1 User Description
static uint8 tmp112ProfileChar1UserDesp[5] = "Amor\0";

/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t tmp112ProfileAttrTbl[SERVAPP_NUM_ATTR_SUPPORTED] =
{
    // Tmp112 Profile Service
    {
        { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
        GATT_PERMIT_READ,                         /* permissions */
        0,                                        /* handle */
        (uint8 *) &tmp112ProfileService           /* pValue */
    },

    // Characteristic 1 Declaration
    {
        { ATT_BT_UUID_SIZE, characterUUID },
        GATT_PERMIT_READ,
        0,
        &tmp112ProfileChar1Props
    },

    // Characteristic Value 1
    {
        { ATT_UUID_SIZE, tmp112Profilechar1UUID },
        GATT_PERMIT_WRITE | GATT_PERMIT_READ,
        0,
        tmp112ProfileChar1
    },

    // Characteristic 1 User Description
    {
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0,
        tmp112ProfileChar1UserDesp
    },

};


/*********************************************************************
 * LOCAL FUNCTIONS
 */
static uint8 tmp112Profile_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                       uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen , uint8 method);
static bStatus_t tmp112Profile_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
        uint8 *pValue, uint8 len, uint16 offset , uint8 method);

static void tmp112Profile_HandleConnStatusCB( uint16 connHandle, uint8 changeType );


/*********************************************************************
 * PROFILE CALLBACKS
 */
// Tmp112 Profile Service Callbacks
CONST gattServiceCBs_t tmp112ProfileCBs =
{
    tmp112Profile_ReadAttrCB,  // Read callback function pointer
    tmp112Profile_WriteAttrCB, // Write callback function pointer
    NULL                       // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      Tmp112Profile_AddService
 *
 * @brief   Initializes the Tmp112 Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t Tmp112Profile_AddService( uint32 services )
{
    uint8 status = SUCCESS;

    // Initialize Client Characteristic Configuration attributes
    //GATTServApp_InitCharCfg( INVALID_CONNHANDLE, tmp112ProfileChar4Config );

    // Register with Link DB to receive link status change callback
    VOID linkDB_Register( tmp112Profile_HandleConnStatusCB );

    if ( services & TMP112PROFILE_SERVICE )
    {
        // Register GATT attribute list and CBs with GATT Server App
        status = GATTServApp_RegisterService( tmp112ProfileAttrTbl,
                                              GATT_NUM_ATTRS( tmp112ProfileAttrTbl ),
                                              GATT_MAX_ENCRYPT_KEY_SIZE,
                                              &tmp112ProfileCBs );
    }

    return ( status );
}


/*********************************************************************
 * @fn      Tmp112Profile_RegisterAppCBs
 *
 * @brief   Registers the application callback function. Only call
 *          this function once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
bStatus_t Tmp112Profile_RegisterAppCBs( tmp112ProfileCBs_t *appCallbacks )
{
    if ( appCallbacks )
    {
        tmp112Profile_AppCBs = appCallbacks;

        return ( SUCCESS );
    }
    else
    {
        return ( bleAlreadyInRequestedMode );
    }
}


/*********************************************************************
 * @fn      Tmp112Profile_SetParameter
 *
 * @brief   Set a Tmp112 Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to right
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t Tmp112Profile_SetParameter( uint8 param, uint8 len, void *value )
{
    bStatus_t ret = SUCCESS;
    switch ( param )
    {
    case TMP112PROFILE_CHAR1:
        if ( len <= TMP112PROFILE_CHAR1_LEN )
        {
            VOID osal_memcpy( tmp112ProfileChar1, value, len );
        }
        else
        {
            ret = bleInvalidRange;
        }
        break;

    default:
        ret = INVALIDPARAMETER;
        break;
    }

    return ( ret );
}

/*********************************************************************
 * @fn      Tmp112Profile_GetParameter
 *
 * @brief   Get a Tmp112 Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate
 *          data type (example: data type of uint16 will be cast to
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t Tmp112Profile_GetParameter( uint8 param, void *value )
{
    bStatus_t ret = SUCCESS;
    switch ( param )
    {
    case TMP112PROFILE_CHAR1:
        VOID osal_memcpy( value, tmp112ProfileChar1, TMP112PROFILE_CHAR1_LEN);
        break;

    default:
        ret = INVALIDPARAMETER;
        break;
    }

    return ( ret );
}

/*********************************************************************
 * @fn          tmp112Profile_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 *
 * @return      Success or Failure
 */
static uint8 tmp112Profile_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                       uint8 *pValue, uint8 *pLen, uint16 offset, uint8 maxLen ,uint8 method)
{
    bStatus_t status = SUCCESS;

    // If attribute permissions require authorization to read, return error
    if ( gattPermitAuthorRead( pAttr->permissions ) )
    {
        // Insufficient authorization
        return ( ATT_ERR_INSUFFICIENT_AUTHOR );
    }

    // Make sure it's not a blob operation (no attributes in the profile are long)
    if ( offset > 0 )
    {
        return ( ATT_ERR_ATTR_NOT_LONG );
    }

    if ( pAttr->type.len == ATT_BT_UUID_SIZE )
    {
#if 0
        // 16-bit UUID
        uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
        switch ( uuid )
        {
            // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
            // gattserverapp handles those reads

            // characteristics 1 and 2 have read permissions
            // characteritisc 3 does not have read permissions; therefore it is not
            //   included here
            // characteristic 4 does not have read permissions, but because it
            //   can be sent as a notification, it is included here
        case GLIESEPROFILE_CHAR1_UUID:
        case GLIESEPROFILE_CHAR2_UUID:
        case GLIESEPROFILE_CHAR4_UUID:
            *pLen = 1;
            pValue[0] = *pAttr->pValue;
            break;

        case GLIESEPROFILE_CHAR5_UUID:
            *pLen = GLIESEPROFILE_CHAR5_LEN;
            VOID osal_memcpy( pValue, pAttr->pValue, GLIESEPROFILE_CHAR5_LEN );
            break;

        default:
            // Should never get here! (characteristics 3 and 4 do not have read permissions)
            *pLen = 0;
            status = ATT_ERR_ATTR_NOT_FOUND;
            break;
        }
#endif
    }
    else
    {
        // 128-bit UUID
        if (osal_memcmp(tmp112Profilechar1UUID, pAttr->type.uuid, 16) == TRUE)
        {
            *pLen = TMP112PROFILE_CHAR1_LEN;
            VOID osal_memcpy( pValue, pAttr->pValue, TMP112PROFILE_CHAR1_LEN );
        }
        else
        {
            // Should never get here!
            *pLen = 0;
            status = ATT_ERR_ATTR_NOT_FOUND;
        }
    }

    return ( status );
}

/*********************************************************************
 * @fn      tmp112Profile_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 *
 * @return  Success or Failure
 */
static bStatus_t tmp112Profile_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
        uint8 *pValue, uint8 len, uint16 offset,uint8 method )
{
    bStatus_t status = SUCCESS;
    uint8 notifyApp = 0xFF;

    // If attribute permissions require authorization to write, return error
    if ( gattPermitAuthorWrite( pAttr->permissions ) )
    {
        // Insufficient authorization
        return ( ATT_ERR_INSUFFICIENT_AUTHOR );
    }

    if ( pAttr->type.len == ATT_BT_UUID_SIZE )
    {
        // 16-bit UUID
        uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
        switch ( uuid )
        {
        case GATT_CLIENT_CHAR_CFG_UUID:
            status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                     offset, GATT_CLIENT_CFG_NOTIFY );
            break;

        default:
            // Should never get here!
            status = ATT_ERR_ATTR_NOT_FOUND;
            break;
        }
    }
    else
    {
        // 128-bit UUID
        //status = ATT_ERR_INVALID_HANDLE;
        if (osal_memcmp(tmp112Profilechar1UUID, pAttr->type.uuid, 16) == TRUE)
        {
            //Validate the value
            // Make sure it's not a blob oper
            if ( offset == 0 )
            {
                if ( len > TMP112PROFILE_CHAR1_LEN )
                {
                    status = ATT_ERR_INVALID_VALUE_SIZE;
                }
            }
            else
            {
                status = ATT_ERR_ATTR_NOT_LONG;
            }
            // Write the value
            if ( status == SUCCESS )
            {
                osal_memset(tmp112ProfileChar1, 0, TMP112PROFILE_CHAR1_LEN);
                VOID osal_memcpy( tmp112ProfileChar1, pValue, len );
                notifyApp = TMP112PROFILE_CHAR1;
            }
        }
        else
        {
            // Should never get here!
            status = ATT_ERR_ATTR_NOT_FOUND;
        }
    }

    // If a charactersitic value changed then callback function to notify application of change
    if ( (notifyApp != 0xFF ) && tmp112Profile_AppCBs && tmp112Profile_AppCBs->pfnTmp112ProfileChange )
    {
        tmp112Profile_AppCBs->pfnTmp112ProfileChange( notifyApp );
    }

    return ( status );
}

/*********************************************************************
 * @fn          tmp112Profile_HandleConnStatusCB
 *
 * @brief       Tmp112 Profile link status change handler function.
 *
 * @param       connHandle - connection handle
 * @param       changeType - type of change
 *
 * @return      none
 */
static void tmp112Profile_HandleConnStatusCB( uint16 connHandle, uint8 changeType )
{
    // Make sure this is not loopback connection
    if ( connHandle != LOOPBACK_CONNHANDLE )
    {
        // Reset Client Char Config if connection has dropped
        if ( ( changeType == LINKDB_STATUS_UPDATE_REMOVED )      ||
                ( ( changeType == LINKDB_STATUS_UPDATE_STATEFLAGS ) &&
                  ( !linkDB_Up( connHandle ) ) ) )
        {
            //GATTServApp_InitCharCfg( connHandle, tmp112ProfileChar4Config );
        }
    }
}
