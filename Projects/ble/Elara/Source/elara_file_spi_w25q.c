/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "hal_adc.h"
#include "hal_flash.h"
#include "hal_types.h"
#include "comdef.h"
#include "OSAL.h"
#include "osal_snv.h"
#include "hal_assert.h"
#include "saddr.h"
#include "elara_error.h"
#include "hal_uart.h"
/*********************************************************************
 * CONSTANTS
 */

// NV page configuration
#define OSAL_NV_PAGE_SIZE       HAL_FLASH_PAGE_SIZE
#define OSAL_NV_PAGES_USED      HAL_NV_PAGE_CNT
#define OSAL_NV_PAGE_BEG        HAL_NV_PAGE_BEG
#define OSAL_NV_PAGE_END       (OSAL_NV_PAGE_BEG + OSAL_NV_PAGES_USED - 1)

// Default byte value when flash is erased
#define OSAL_NV_ERASED          0xFF

// NV page header size in bytes
#define OSAL_NV_PAGE_HDR_SIZE  4

// In case pages 0-1 are ever used, define a null page value.
#define OSAL_NV_PAGE_NULL       0

// In case item Id 0 is ever used, define a null item value.
#define OSAL_NV_ITEM_NULL       0

// Length in bytes of a flash word
#define OSAL_NV_WORD_SIZE       HAL_FLASH_WORD_SIZE

// NV page header offset within a page
#define OSAL_NV_PAGE_HDR_OFFSET 0


// Flag in a length field of an item header to indicate validity
// of the length field
#define OSAL_NV_INVALID_LEN_MARK 0x8000

// Flag in an ID field of an item header to indicate validity of
// the identifier field
#define OSAL_NV_INVALID_ID_MARK  0x8000


// Bit difference between active page state indicator value and
// transfer page state indicator value
#define OSAL_NV_ACTIVE_XFER_DIFF  0x00100000

// active page state indicator value
#define OSAL_NV_ACTIVE_PAGE_STATE OSAL_NV_ACTIVE_XFER_DIFF

// transfer page state indicator value
#define OSAL_NV_XFER_PAGE_STATE   (OSAL_NV_ACTIVE_PAGE_STATE ^ OSAL_NV_ACTIVE_XFER_DIFF)

#define OSAL_NV_MIN_COMPACT_THRESHOLD   70 // Minimum compaction threshold
#define OSAL_NV_MAX_COMPACT_THRESHOLD   95 // Maximum compaction threshold

/*********************************************************************
 * MACROS
 */
#define ELARA_PAGE_BEGIN 81
//Should be 124
//#define ELARA_PAGE_END (HAL_NV_PAGE_BEG-1)
#define ELARA_PAGE_END 123
//#define ELARA_PAGE_END 95

/********************************************************************/
// For more command: http://www.winbond.com
#define XNV_STAT_CMD  0x05
#define XNV_WREN_CMD  0x06
#define XNV_WRPG_CMD  0x02
#define XNV_READ_CMD  0x03
#define XNV_CHIP_ERASE_CMD      0x60 // or 0x60
#define XNV_SECTOR_ERASE_CMD    0x20
#define XNV_32K_BLOCK_ERASE_CMD 0x52
#define XNV_64K_BLOCK_ERASE_CMD 0xD8

#define XNV_STAT_BUSY  0x01

/* ----------- XNV ---------- */
//#define SPI_BEGIN()             st(P1_4 = 0;)
#define SPI_BEGIN()             st(P1_2 = 0;)
//#define SPI_END()               st(P1_4 = 1;)
#define SPI_END()               st(P1_2 = 1;)
#define SPI_TX(x)               st(U1CSR &= ~0x02; U1DBUF = (x);)
#define SPI_RX()                U1DBUF
#define SPI_WAIT_RXRDY()        st(while (!(U1CSR & 0x02));)

#if 0
#define SPI_WRITE_BYTE(ch) \
st( \
    U1CSR &= ~0x02; \
    U1DBUF = (ch); \
    while (!(U1CSR & 0x02)); \
)
#endif

//#define SPI_FLASH_SIZE              1048576 // 8Mbit/1MB
//#define SPI_FLASH_SIZE              2097152 // 16Mbit/2MB
//#define SPI_FLASH_SIZE              4194304 // 32Mbit/4MB
//#define SPI_FLASH_SIZE              8388608 // 64Mbit/8MB
#define SPI_FLASH_SIZE              16777216 // 128Mbit/16MB

#define SPI_PAGE_SIZE               256
#define SPI_SECTOR_SIZE             4096 // 4KB
#define SPI_BLOCK_SIZE              32768 // 32KB

#define SPI_ADDRESS_BEGIN           0

/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * EXTERNAL FUNCTIONS
 */


/*********************************************************************
 * GLOBAL VARIABLES
 */
/*********************************************************************
 * LOCAL VARIABLES
 */
static uint32 g_AddressBegin = 0;
static uint32 g_AddressActive = 0;

static uint32 g_bin_size = 0;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
// To see 'XNV_SPI_INIT'
// The TI reference design uses UART1 Alt. 2 in SPI mode.
static void elara_spi_init1(void)
{
  /* Mode select UART1 SPI Mode as master. */
  U1CSR = 0;
#if 0
  /* Setup for 115200 baud. */
  U1GCR = 11; 
  U1BAUD = 216; 
#elif 0
  U1GCR = 12; 
  U1BAUD = 216;   
#elif 1
  U1GCR = 15; 
  U1BAUD = 255;   
#else
    // SCK frequency = 3MHz (CC254x max = 4MHz)
  U1GCR = 0x10; 
  U1BAUD = 0x80;   
#endif

  /* Set bit order to MSB */
  U1GCR |= BV(5); 
  
  /* Set UART1 I/O to alternate 2 location on P1 pins. */
  PERCFG |= 0x02;  /* U1CFG */
  
  /* Select peripheral function on I/O pins but SS is left as GPIO for separate control. */
  P1SEL |= 0xE0;  /* SELP1_[7:4] */
  /* P1.1,2,3: reset, LCD CS, XNV CS. */
//  P1SEL &= ~0x0E; 
  P1 |= 0x0E; 
  P1_1 = 0; 
  P1DIR |= 0x0E; 
  
  /* Give UART1 priority over Timer3. */
  P2SEL &= ~0x20;  /* PRI2P1 */
  
  /* When SPI config is complete, enable it. */
  U1CSR |= 0x40; 
  /* Release XNV reset. */
  P1_1 = 1; 
}

/*
 * UART 0 SPI Alt.2
 */
static void elara_spi_init2(void)
{

  PERCFG |= 0x01;     /* Set UART0 I/O to Alt. 2 location on P1 */

  P1SEL |= 0x38;        /* SPI-Master peripheral select */
  
  U0CSR = 0;                         /* Mode is SPI-Master Mode */
  U0UCR |= 0x80;                 /* Flush it */
  
  U0GCR =  15;                       /* Cfg for the max Rx/Tx baud of 2-MHz */
  U0BAUD = 255;

  
  U0GCR |= BV(5);                    /* Set bit order to MSB */
  //U1GCR |= BV(6);
  //U1GCR |= BV(7);

  //P2DIR &= ~0xC0;
  //P2DIR |= 0x40;/* USART1 priority over UART0 */

  /* Setup GPIO for interrupts by falling edge on SPI_RDY_IN */
//  PxIEN |= SPI_RDYIn_BIT;
//  PICTL |= PICTL_BIT;

  P1_2 = 1;
  P1DIR |= BV(2);

  U0CSR |= 0x40;

  //PxIFG = 0;
  //PxIF = 0;
  //IENx |= IEN_BIT;
}

static void elara_spi_init(void)
{

  PERCFG |= 0x02;     /* Set UART1 I/O to Alt. 2 location on P1 */

  P1SEL |= 0xE0;        /* SPI-Master peripheral select */
  U1CSR = 0;                         /* Mode is SPI-Master Mode */
  U1UCR |= 0x80;                 /* Flush it */
  
  U1GCR =  15;                       /* Cfg for the max Rx/Tx baud of 2-MHz */
  U1BAUD = 255;

  
  U1GCR |= BV(5);                    /* Set bit order to MSB */
  //U1GCR |= BV(6);
  //U1GCR |= BV(7);

  P2DIR &= ~0xC0;
  P2DIR |= 0x40;/* USART1 priority over UART0 */

  /* Setup GPIO for interrupts by falling edge on SPI_RDY_IN */
//  PxIEN |= SPI_RDYIn_BIT;
//  PICTL |= PICTL_BIT;

  P1_4 = 1;
  P1DIR |= BV(4);

  U1CSR |= 0x40;

  //PxIFG = 0;
  //PxIF = 0;
  //IENx |= IEN_BIT;
}

#if 0
static void SPIWrite(uint8 ch)
{
  SPI_TX(ch);
  SPI_WAIT_RXRDY();
}
#endif

static void SPI_WRITE_BYTE(uint8 ch)
{
  XNV_SPI_TX(ch);
  XNV_SPI_WAIT_RXRDY();
}

static void HalSPIRead(uint32 addr, uint8 *pBuf, uint16 len)
{

  //uint8 shdw = P1DIR;
  //P1DIR |= BV(3);
  
  SPI_BEGIN();
  do {
    SPI_WRITE_BYTE(XNV_STAT_CMD);
  } while (SPI_RX() & XNV_STAT_BUSY);
  SPI_END();
  asm("NOP"); asm("NOP");

  SPI_BEGIN();
  SPI_WRITE_BYTE(XNV_READ_CMD);
  SPI_WRITE_BYTE(addr >> 16);
  SPI_WRITE_BYTE(addr >> 8);
  SPI_WRITE_BYTE(addr);
  //SPI_WRITE_BYTE(0);

  while (len--)
  {
    SPI_WRITE_BYTE(0);
    *pBuf++ = SPI_RX();
    //while (!(U1CSR & 0x02));
  }
  SPI_END();

  //P1DIR = shdw;
}

static void HalSPIWrite(uint32 addr, uint8 *pBuf, uint16 len)
{
  uint8 cnt;
//  uint8 shdw = P1DIR;
//  P1DIR |= BV(3);
  while (len)
  {
    SPI_BEGIN();
    do {
      SPI_WRITE_BYTE(XNV_STAT_CMD);
    } while (SPI_RX() & XNV_STAT_BUSY);
    SPI_END();
    asm("NOP"); asm("NOP");

    SPI_BEGIN();
    SPI_WRITE_BYTE(XNV_WREN_CMD);
    SPI_END();
    asm("NOP"); asm("NOP");

    SPI_BEGIN();
    SPI_WRITE_BYTE(XNV_WRPG_CMD);
    SPI_WRITE_BYTE(addr >> 16);
    SPI_WRITE_BYTE(addr >> 8);
    SPI_WRITE_BYTE(addr);

    // Can only write within any one page boundary, so prepare for next page write if bytes remain.
    cnt = 0 - (uint8)addr;
    if (cnt)
    {
      addr += cnt;
    }
    else
    {
      addr += 256;
    }

    do
    {
      SPI_WRITE_BYTE(*pBuf++);
      cnt--;
      len--;
    } while (len && cnt);
    SPI_END();
  }
  //P1DIR = shdw;
}

static void HalSPIChipErase(void)
{
    SPI_BEGIN();
    do {
      SPI_WRITE_BYTE(XNV_STAT_CMD);
    } while (SPI_RX() & XNV_STAT_BUSY);
    SPI_END();
    asm("NOP"); asm("NOP");

    SPI_BEGIN();
    SPI_WRITE_BYTE(XNV_WREN_CMD);
    SPI_END();
    asm("NOP"); asm("NOP");

    SPI_BEGIN();
    SPI_WRITE_BYTE(XNV_CHIP_ERASE_CMD);
    SPI_END();
}
/* Ative delay: 125 cycles ~1 msec */
#define W25Q_DELAY(n) st( { volatile uint32 i; for (i=0; i<(n); i++) { }; } )
static void spiSerialAppCallback(uint8 port, uint8 event)
{
    //ELARA_LED2 = 1;
    //ELARA_LED1 = 1;
}
int test_spi_get_status_register1(uint8 *pBuf, uint8 len)
{
    HalUARTInit();
    halUARTCfg_t uartConfig;
    uartConfig.callBackFunc = spiSerialAppCallback;
    (void)HalUARTOpen( 1, &uartConfig );

    uint8 ch = XNV_WREN_CMD;
    int rs = HalUARTWrite (1, &ch, 1);
    W25Q_DELAY(12500);//100ms

    P0_1 = 1; // Orange LED.
    ELARA_LED1 = 1;
    ch = XNV_STAT_CMD;
    rs = HalUARTWrite (1, &ch, 1);
    W25Q_DELAY(125000);//100ms
    HalUARTRead (1, pBuf, 1);
    

    ch = 0x35;
    rs = HalUARTWrite (1, &ch, 1);
    //W25Q_DELAY(12500);//100ms
    HalUARTRead (1, pBuf+1, 1);

    ch = 0x15;
    rs = HalUARTWrite (1, &ch, 1);
    //W25Q_DELAY(12500);//100ms
    HalUARTRead (1, pBuf+2, 1);

    return 0;
}

int test_spi_get_status_register(uint8 *pBuf, uint8 len)
{
    elara_spi_init2();
    ELARA_LED2 = 1;
    ELARA_LED1 = 1;

    SPI_BEGIN();
    SPI_WRITE_BYTE(XNV_WREN_CMD);
    SPI_END();
    asm("NOP"); asm("NOP");
    
    SPI_BEGIN();
    SPI_WRITE_BYTE(XNV_STAT_CMD);
    *pBuf++ = SPI_RX();
    SPI_END();
    asm("NOP"); asm("NOP");asm("NOP"); asm("NOP");

    SPI_BEGIN();
    SPI_WRITE_BYTE(0x35);
    *pBuf++ = SPI_RX();
    SPI_END();
    asm("NOP"); asm("NOP");asm("NOP"); asm("NOP");

    SPI_BEGIN();
    SPI_WRITE_BYTE(0x15);
    *pBuf++ = SPI_RX();
    SPI_END();
    asm("NOP"); asm("NOP");asm("NOP"); asm("NOP");

    //ELARA_LED2 = 0;
    //ELARA_LED1 = 0;
    return 0;
}


/*********************************************************************
*********************************************************************/
