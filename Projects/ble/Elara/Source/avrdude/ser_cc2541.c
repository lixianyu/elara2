/*
 * avrdude - A Downloader/Uploader for AVR device programmers
 * Copyright (C) 2003-2004  Theodore A. Roth  <troth@openavr.org>
 * Copyright (C) 2006 Joerg Wunsch <j@uriah.heep.sax.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* $Id: ser_posix.c 1294 2014-03-12 23:03:18Z joerg_wunsch $ */

/*
 * Posix serial interface for avrdude.
 */


#include "avrdude.h"
#include "serial.h"

/* Ative delay: 125 cycles ~1 msec */
#define SERIAL_DELAY(n) st( { volatile uint32 i; for (i=0; i<(n); i++) { }; } )


long serial_recv_timeout = 5000; /* ms */
#if 0
struct baud_mapping
{
    long baud;
    speed_t speed;
};

/* There are a lot more baud rates we could handle, but what's the point? */

static struct baud_mapping baud_lookup_table [] =
{
    { 1200,   B1200 },
    { 2400,   B2400 },
    { 4800,   B4800 },
    { 9600,   B9600 },
    { 19200,  B19200 },
    { 38400,  B38400 },
#ifdef B57600
    { 57600,  B57600 },
#endif
#ifdef B115200
    { 115200, B115200 },
#endif
#ifdef B230400
    { 230400, B230400 },
#endif
    { 0,      0 }                 /* Terminator. */
};

static struct termios original_termios;
static int saved_original_termios;

static speed_t serial_baud_lookup(long baud)
{
    struct baud_mapping *map = baud_lookup_table;

    while (map->baud)
    {
        if (map->baud == baud)
            return map->speed;
        map++;
    }

    /*
     * If a non-standard BAUD rate is used, issue
     * a warning (if we are verbose) and return the raw rate
     */
    if (verbose > 0)
        fprintf(stderr, "%s: serial_baud_lookup(): Using non-standard baud rate: %ld",
                progname, baud);

    return baud;
}
#endif

static int ser_setspeed(union filedescriptor *fd, long baud)
{


    return 0;
}

/*
 * Given a port description of the form <host>:<port>, open a TCP
 * connection to the specified destination, which is assumed to be a
 * terminal/console server with serial parameters configured
 * appropriately (e. g. 115200-8-N-1 for a STK500.)
 */
static int
net_open(const char *port, union filedescriptor *fdp)
{

    return 0;
}


static int ser_set_dtr_rts(union filedescriptor *fdp, int is_on)
{
    P0_6 = is_on;
    return 0;
}

static int ser_open(char *port, union pinfo pinfo, union filedescriptor *fdp)
{

    return 0;
}


static void ser_close(union filedescriptor *fd)
{

}


static int ser_send(union filedescriptor *fd, unsigned char *buf, uint32 buflen)
{

    int rs = HalUARTWrite (0, buf, buflen);
    //SERIAL_DELAY(6250);//50ms
    SERIAL_DELAY(12500);//100ms
    //SERIAL_DELAY(131250);
    return rs;
}

#if 0
static int ser_recv(union filedescriptor *fd, unsigned char *buf, uint32 buflen)
{
    int tries = 0;
    uint16 rc = 0;
    //SERIAL_DELAY(6250);
retrys:
    rc = HalUARTRead (0, buf, buflen);
    if (rc == 0)
    {
        if (tries++ > 10)
        {
            return 0;
        }
        else
        {
            SERIAL_DELAY(6250);
            goto retrys;
        }
    }
    else
    {
        return rc;
    }
}
#elif 1
static int ser_recv(union filedescriptor *fd, unsigned char *buf, uint32 buflen)
{
    return HalUARTRead (0, buf, buflen);
}
#else
static int ser_recv(union filedescriptor *fd, unsigned char *buf, uint32 buflen)
{
    uint16 numBytes = Hal_UART_RxBufLen(0);
    P1_4 = 1;
    while (numBytes == 0)
    {
        SERIAL_DELAY(6250);
        numBytes = Hal_UART_RxBufLen(0);
    }
    P1_4 = 0;
    uint16 rc = HalUARTRead (0, buf, buflen);
    return rc;
}

#endif

static int ser_drain(union filedescriptor *fd, int display)
{
    #if 1
    uint8 drainBuf[64];
    HalUARTRead (0, drainBuf, sizeof(drainBuf));
    #endif
    return 0;
}

struct serial_device serial_serdev =
{
    .open = ser_open,
    .setspeed = ser_setspeed,
    .close = ser_close,
    .send = ser_send,
    .recv = ser_recv,
    .drain = ser_drain,
    .set_dtr_rts = ser_set_dtr_rts,
    .flags = 0,//SERDEV_FL_CANSETSPEED,
};

struct serial_device *serdev = &serial_serdev;

