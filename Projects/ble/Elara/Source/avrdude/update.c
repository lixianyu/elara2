/*
 * avrdude - A Downloader/Uploader for AVR device programmers
 * Copyright (C) 2000-2005  Brian S. Dean <bsd@bsdhome.com>
 * Copyright (C) 2007 Joerg Wunsch
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/* $Id: update.c 1294 2014-03-12 23:03:18Z joerg_wunsch $ */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
//#include <time.h>

#include "bcomdef.h"
#include "elara_file_common.h"
#include "avrdude.h"
#include "avr.h"
//#include "config.h"
//#include "confwin.h"
#include "fileio.h"
#include "update.h"
#include "OSAL_Memory.h"
#include "..\SerialApp.h"
#include "stk500.h"
static void delay_nus(uint32 timeout)
{
    while (timeout--)
    {
        asm("NOP");
        asm("NOP");
        asm("NOP");
    }
}

int8 parse_op_elara(UPDATE *upd)
{
    //SerialPrintValue("avr_init:BLE used memory is : ", osal_heap_mem_used(), 10);
    SerialPrintString("111............\r\n");
    //delay_nus(100000);
    
    
    strcpy(upd->memtype, "flash");
    upd->op = DEVICE_WRITE;
    upd->format = FMT_IHEX;
    strcpy(upd->filename, "Blink.hex");
    SerialPrintString("222............\r\n");
    return 0;
}

#if 0
UPDATE *parse_op(char *s)
{
    char buf[32];
    char *p, * cp, c;
    UPDATE *upd;
    int i;
    size_t fnlen;

    //SerialPrintValue("avr_init:BLE used memory is : ", osal_heap_mem_used(), 10);
    SerialPrintString("000............\r\n");
    //delay_nus(100000);
    SerialPrintString("111............\r\n");
    upd = (UPDATE *)osal_mem_alloc(sizeof(UPDATE));

    if (upd == NULL)
    {
        //fprintf(stderr, "%s: out of memory\n", progname);
        //exit(1);
    }
    
    i = 0;
    p = s;
    while ((i < (sizeof(buf) - 1) && *p && (*p != ':')))
        buf[i++] = *p++;
    buf[i] = 0;

    if (*p != ':')
    {
        upd->memtype = NULL;        /* default memtype, "flash", or "application" */
        upd->op = DEVICE_WRITE;
        upd->filename = (char *)osal_mem_alloc(strlen(buf) + 1);
        if (upd->filename == NULL)
        {
            //fprintf(stderr, "%s: out of memory\n", progname);
            //exit(1);
        }
        strcpy(upd->filename, buf);
        upd->format = FMT_AUTO;
        return upd;
    }

    upd->memtype = (char *)osal_mem_alloc(strlen(buf) + 1);
    if (upd->memtype == NULL)
    {
        //fprintf(stderr, "%s: out of memory\n", progname);
        //exit(1);
    }
    strcpy(upd->memtype, buf);

    p++;
    if (*p == 'r')
    {
        upd->op = DEVICE_READ;
    }
    else if (*p == 'w')
    {
        upd->op = DEVICE_WRITE;
    }
    else if (*p == 'v')
    {
        upd->op = DEVICE_VERIFY;
    }
    else
    {
#if 0
        fprintf(stderr, "%s: invalid I/O mode '%c' in update specification\n",
                progname, *p);
        fprintf(stderr,
                "  allowed values are:\n"
                "    r = read device\n"
                "    w = write device\n"
                "    v = verify device\n");
#endif
        osal_mem_free(upd->memtype);
        osal_mem_free(upd);
        return NULL;
    }

    p++;

    if (*p != ':')
    {
        //fprintf(stderr, "%s: invalid update specification\n", progname);
        osal_mem_free(upd->memtype);
        osal_mem_free(upd);
        return NULL;
    }

    p++;

    /*
     * Now, parse the filename component.  Instead of looking for the
     * leftmost possible colon delimiter, we look for the rightmost one.
     * If we found one, we do have a trailing :format specifier, and
     * process it.  Otherwise, the remainder of the string is our file
     * name component.  That way, the file name itself is allowed to
     * contain a colon itself (e. g. C:/some/file.hex), except the
     * optional format specifier becomes mandatory then.
     */
    cp = p;
    p = strrchr(cp, ':');
    if (p == NULL)
    {
        upd->format = FMT_AUTO;
        fnlen = strlen(cp);
        upd->filename = (char *)osal_mem_alloc(fnlen + 1);
    }
    else
    {
        fnlen = p - cp;
        upd->filename = (char *)osal_mem_alloc(fnlen + 1);
        c = *++p;
        if (c && p[1])
            /* More than one char - force failure below. */
            c = '?';
        switch (c)
        {
        case 'a':
            upd->format = FMT_AUTO;
            break;
        case 's':
            upd->format = FMT_SREC;
            break;
        case 'i':
            upd->format = FMT_IHEX;
            break;
        case 'r':
            upd->format = FMT_RBIN;
            break;
        case 'e':
            upd->format = FMT_ELF;
            break;
        case 'm':
            upd->format = FMT_IMM;
            break;
        case 'b':
            upd->format = FMT_BIN;
            break;
        case 'd':
            upd->format = FMT_DEC;
            break;
        case 'h':
            upd->format = FMT_HEX;
            break;
        case 'o':
            upd->format = FMT_OCT;
            break;
        default:
            //fprintf(stderr, "%s: invalid file format '%s' in update specifier\n", progname, p);
            osal_mem_free(upd->memtype);
            osal_mem_free(upd);
            return NULL;
        }
    }

    if (upd->filename == NULL)
    {
        //fprintf(stderr, "%s: out of memory\n", progname);
        osal_mem_free(upd->memtype);
        osal_mem_free(upd);
        return NULL;
    }
    memcpy(upd->filename, cp, fnlen);
    upd->filename[fnlen] = 0;

    return upd;
}
#endif

char *strdup(char *str)
{
    char *strNew;

    strNew = (char *)osal_mem_alloc(strlen(str) + 1);
    strcpy(strNew, str);
    return strNew;
}

#if 0
UPDATE *dup_update(UPDATE *upd)
{
    UPDATE *u;

    u = (UPDATE *)osal_mem_alloc(sizeof(UPDATE));
    if (u == NULL)
    {
        //fprintf(stderr, "%s: out of memory\n", progname);
        //exit(1);
    }

    memcpy(u, upd, sizeof(UPDATE));

    if (upd->memtype != NULL)
        u->memtype = (char *)strdup(upd->memtype);
    else
        u->memtype = NULL;
    u->filename = (char *)strdup(upd->filename);

    return u;
}

UPDATE *new_update(int op, char *memtype, int filefmt, char *filename)
{
    UPDATE *u;

    u = (UPDATE *)osal_mem_alloc(sizeof(UPDATE));
    if (u == NULL)
    {
        //fprintf(stderr, "%s: out of memory\n", progname);
        //exit(1);
    }

    u->memtype = (char *)strdup(memtype);
    u->filename = (char *)strdup(filename);
    u->op = op;
    u->format = filefmt;

    return u;
}
#endif

void free_update(UPDATE *u)
{
    #if 0
    if (u != NULL)
    {
        if(u->memtype != NULL)
        {
            osal_mem_free(u->memtype);
            u->memtype = NULL;
        }
        if(u->filename != NULL)
        {
            osal_mem_free(u->filename);
            u->filename = NULL;
        }
        osal_mem_free(u);
    }
    #endif
}


int do_op(PROGRAMMER *pgm, struct avrpart *p, UPDATE *upd, enum updateflags flags)
{
    struct avrpart *v;
    AVRMEM *mem;
    int32 size, vsize;
    int rc;

    SerialPrintString("Enter do_op()\r\n");
    mem = avr_locate_mem(p, upd->memtype);
    if (mem == NULL)
    {
        //fprintf(stderr, "\"%s\" memory type not defined for part \"%s\"\n",
        //        upd->memtype, p->desc);
        return STK_ERROR_CMD_87;
    }

    if (upd->op == DEVICE_READ)
    {
        /*
         * read out the specified device memory and write it to a file
         */
        if (quell_progress < 2)
        {
            //fprintf(stderr, "%s: reading %s memory:\n",
            //      progname, mem->desc);
        }
        //report_progress(0,1,"Reading");
        rc = avr_read(pgm, p, upd->memtype, 0);
        if (rc < 0)
        {
            //fprintf(stderr, "%s: failed to read all of %s memory, rc=%d\n",
            //        progname, mem->desc, rc);
            return STK_ERROR_CMD_88;
        }
        //report_progress(1,1,NULL);
        size = rc;

        if (quell_progress < 2)
        {
            //fprintf(stderr,
            //      "%s: writing output file \"%s\"\n",
            //      progname,
            //      strcmp(upd->filename, "-")==0 ? "<stdout>" : upd->filename);
        }
        //rc = fileio(FIO_WRITE, upd->filename, upd->format, p, upd->memtype, size);
        if (rc < 0)
        {
            //fprintf(stderr, "%s: write to file '%s' failed\n",
            //        progname, upd->filename);
            return STK_ERROR_CMD_89;
        }
    }
    else if (upd->op == DEVICE_WRITE)  //Should go into here -- lxy added.
    {
        /*
         * write the selected device memory using data from a file; first
         * read the data from the specified file
         */
        if (quell_progress < 2)
        {
            //fprintf(stderr,
            //      "%s: reading input file \"%s\"\n",
            //      progname,
            //      strcmp(upd->filename, "-")==0 ? "<stdin>" : upd->filename);
        }
#if 0
        rc = fileio(FIO_READ, upd->filename, upd->format, p, upd->memtype, -1);
#else
        //TODO
        rc = 3;
#endif
        if (rc < 0)
        {
            //fprintf(stderr, "%s: read from file '%s' failed\n",
            //        progname, upd->filename);
            return STK_ERROR_CMD_90;
        }
        //size = 282; // I got a blink.bin , which size is 282 bytes. ---lxy added.
        uint32 binSize = 0;
        elara_snv_read(BLE_NVID_MCU_BIN_SIZE, 4, &binSize);
        size = binSize;
        /*
         * write the buffer contents to the selected memory type
         */
        if (quell_progress < 2)
        {
            //fprintf(stderr, "%s: writing %s (%d bytes):\n",
            //      progname, mem->desc, size);
        }

        if (!(flags & UF_NOWRITE))   //Should go into here -- lxy added.
        {
            //report_progress(0,1,"Writing");
            rc = avr_write_elara(pgm, p, upd->memtype, size, (flags & UF_AUTO_ERASE) != 0);
            //return rc;
            //report_progress(1,1,NULL);
        }
        else
        {
            /*
             * test mode, don't actually write to the chip, output the buffer
             * to stdout in intel hex instead
             */
            //rc = fileio(FIO_WRITE, "-", FMT_IHEX, p, upd->memtype, size);
        }

        if (rc < 0)
        {
            //fprintf(stderr, "%s: failed to write %s memory, rc=%d\n",
            //        progname, mem->desc, rc);
            //return STK_ERROR_CMD_91;
            return rc;
        }

        vsize = rc;

        if (quell_progress < 2)
        {
            //fprintf(stderr, "%s: %d bytes of %s written\n", progname,
            //      vsize, mem->desc);
        }

    }
    else if (upd->op == DEVICE_VERIFY)
    {
        /*
         * verify that the in memory file (p->mem[AVR_M_FLASH|AVR_M_EEPROM])
         * is the same as what is on the chip
         */
        pgm->vfy_led(pgm, ON);
#if 0
        if (quell_progress < 2)
        {
            fprintf(stderr, "%s: verifying %s memory against %s:\n",
                    progname, mem->desc, upd->filename);

            fprintf(stderr, "%s: load data %s data from input file %s:\n",
                    progname, mem->desc, upd->filename);
        }
#endif
        //rc = fileio(FIO_READ, upd->filename, upd->format, p, upd->memtype, -1);
        if (rc < 0)
        {
            //fprintf(stderr, "%s: read from file '%s' failed\n",
            //        progname, upd->filename);
            return -1;
        }
        v = avr_dup_part(p);
        size = rc;
#if 0
        if (quell_progress < 2)
        {
            fprintf(stderr, "%s: input file %s contains %d bytes\n",
                    progname, upd->filename, size);
            fprintf(stderr, "%s: reading on-chip %s data:\n",
                    progname, mem->desc);
        }
#endif
        //report_progress (0,1,"Reading");
        rc = avr_read(pgm, p, upd->memtype, v);
        if (rc < 0)
        {
            //fprintf(stderr, "%s: failed to read all of %s memory, rc=%d\n",
            //        progname, mem->desc, rc);
            pgm->err_led(pgm, ON);
            return -1;
        }
        //report_progress (1,1,NULL);



        if (quell_progress < 2)
        {
            //fprintf(stderr, "%s: verifying ...\n", progname);
        }
        rc = avr_verify(p, v, upd->memtype, size);
        if (rc < 0)
        {
            //fprintf(stderr, "%s: verification error; content mismatch\n",
            //        progname);
            pgm->err_led(pgm, ON);
            return -1;
        }

        if (quell_progress < 2)
        {
            //fprintf(stderr, "%s: %d bytes of %s verified\n",
            //        progname, rc, mem->desc);
        }

        pgm->vfy_led(pgm, OFF);
    }
    else
    {
        //fprintf(stderr, "%s: invalid update operation (%d) requested\n",
        //        progname, upd->op);
        return STK_ERROR_CMD_92;
    }

    return 0;
}

int do_op_2(PROGRAMMER *pgm, struct avrpart *p, UPDATE *upd, enum updateflags flags)
{
    struct avrpart *v;
    AVRMEM *mem;
    int32 size, vsize;
    int rc;

    SerialPrintString("Enter do_op()\r\n");
    mem = avr_locate_mem(p, upd->memtype);
    if (mem == NULL)
    {
        //fprintf(stderr, "\"%s\" memory type not defined for part \"%s\"\n",
        //        upd->memtype, p->desc);
        return STK_ERROR_CMD_87;
    }

    if (upd->op == DEVICE_WRITE)  //Should go into here -- lxy added.
    {
        /*
         * write the selected device memory using data from a file; first
         * read the data from the specified file
         */
        if (quell_progress < 2)
        {
            //fprintf(stderr,
            //      "%s: reading input file \"%s\"\n",
            //      progname,
            //      strcmp(upd->filename, "-")==0 ? "<stdin>" : upd->filename);
        }
#if 0
        rc = fileio(FIO_READ, upd->filename, upd->format, p, upd->memtype, -1);
#else
        //TODO
        rc = 3;
#endif
        if (rc < 0)
        {
            //fprintf(stderr, "%s: read from file '%s' failed\n",
            //        progname, upd->filename);
            return STK_ERROR_CMD_90;
        }
        //size = 282; // I got a blink.bin , which size is 282 bytes. ---lxy added.
        uint32 binSize = 0;
        elara_snv_read(BLE_NVID_MCU_BIN_SIZE, 4, &binSize);
        size = binSize;
        /*
         * write the buffer contents to the selected memory type
         */
        if (quell_progress < 2)
        {
            //fprintf(stderr, "%s: writing %s (%d bytes):\n",
            //      progname, mem->desc, size);
        }

        if (!(flags & UF_NOWRITE))   //Should go into here -- lxy added.
        {
            //report_progress(0,1,"Writing");
            rc = avr_write_elara_2(pgm, p, upd->memtype, size, (flags & UF_AUTO_ERASE) != 0);
            //return rc;
            //report_progress(1,1,NULL);
        }
        else
        {
            /*
             * test mode, don't actually write to the chip, output the buffer
             * to stdout in intel hex instead
             */
            //rc = fileio(FIO_WRITE, "-", FMT_IHEX, p, upd->memtype, size);
        }

        if (rc < 0)
        {
            //fprintf(stderr, "%s: failed to write %s memory, rc=%d\n",
            //        progname, mem->desc, rc);
            //return STK_ERROR_CMD_91;
            return rc;
        }

        vsize = rc;

        if (quell_progress < 2)
        {
            //fprintf(stderr, "%s: %d bytes of %s written\n", progname,
            //      vsize, mem->desc);
        }

    }
    else
    {
        //fprintf(stderr, "%s: invalid update operation (%d) requested\n",
        //        progname, upd->op);
        return STK_ERROR_CMD_92;
    }

    return 0;
}
