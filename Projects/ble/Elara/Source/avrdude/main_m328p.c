/*********************************************************************
 * INCLUDES
 */
#include "ac_cfg.h"

//#include <stdio.h>
#include <stdlib.h>
//#include <errno.h>
//#include <fcntl.h>
//#include <limits.h>
#include <string.h>
//#include <time.h>
//#include <unistd.h>
//#include <ctype.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <sys/time.h>
#include "OSAL.h"
#include "avr.h"
//#include "config.h"
//#include "confwin.h"
#include "fileio.h"
#include "lists.h"
//#include "par.h"
#include "pindefs.h"
//#include "term.h"
////#include "safemode.h"
#include "update.h"
#include "pgm_type.h"

#include "arduino.h"
/* Get VERSION from ac_cfg.h */
//char * version      = VERSION;
#include "..\SerialApp.h"
#include "..\elara_file_common.h"

#include "main_common.h"

#define fprintf(s, ...)
/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * EXTERNAL FUNCTIONS
 */


/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
static unsigned char const g_eeprom_instr[EEPROM_INSTR_SIZE] = {0xBD, 0xF2, 0xBD, 0xE1, 0xBB, 0xCF, 0xB4, 0x00,
                                                         0xBE, 0x01, 0xB6, 0x01, 0xBC, 0x00, 0xBB, 0xBF,
                                                         0x99, 0xF9, 0xBB, 0xAF
                                                        };
static unsigned char const g_controlstack[CTL_STACK_SIZE] = {0x0E, 0x1E, 0x0F, 0x1F, 0x2E, 0x3E, 0x2F,
                                                      0x3F, 0x4E, 0x5E, 0x4F, 0x5F, 0x6E, 0x7E, 0x6F, 0x7F,
                                                      0x66, 0x76, 0x67, 0x77, 0x6A, 0x7A, 0x6B, 0x7B, 0xBE,
                                                      0xFD, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00
                                                     };
static const OPCODE g_op_part_PGM_ENABLE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,1,0},{1,1,1},{1,0,2},{1,0,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static const OPCODE g_op_part_CHIP_ERASE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
/*********************************************************************
 * LOCAL FUNCTIONS
 */
static const OPCODE g_op_eeprom_read = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static const OPCODE g_op_eeprom_write = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,1,7},
}};
static const OPCODE g_op_eeprom_LOADPAGE_LO = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,1,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,1,7},
}};
static const OPCODE g_op_eeprom_WRITEPAGE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,1,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,1,7},
}};
static uint8 g_eeprom_buf[128];
static uint8 g_eeprom_tags[128];
static void init_memtype_eeprom(AVRMEM *current_mem)
{
    int opnum;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_eeprom()\r\n");

    current_mem->buf = g_eeprom_buf;
    current_mem->tags = g_eeprom_tags;
    
    strncpy(current_mem->desc, "eeprom", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->paged = 0;

    current_mem->page_size = 4;

    current_mem->size = 1024;

    current_mem->min_write_delay = 3600;

    current_mem->max_write_delay = 3600;

    current_mem->readback[0] = 0xff;
    current_mem->readback[1] = 0xff;

    current_mem->mode = 0x41;

    current_mem->delay = 20;

    current_mem->blocksize = 4;

    current_mem->readsize = 256;

    opnum = AVR_OP_READ;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_read;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    
    current_mem->op[opnum] = op;

#if defined(DEBUG_SERIAL)
    uint16 usedmem = 0;
    usedmem = osal_heap_mem_used();
    SerialPrintValue("Now BLE used memory is : ", usedmem, 10);
#endif
    //.........................................
    opnum = AVR_OP_WRITE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_write;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
#if defined(DEBUG_SERIAL)
    usedmem = osal_heap_mem_used();
    SerialPrintValue("Now BLE used memory is : ", usedmem, 10);
#endif
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_LO;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_LOADPAGE_LO;
    SerialPrintValue("AVR_OP_LOADPAGE_LO op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITEPAGE;
    //op = avr_new_opcode();
    op = (OPCODE*)&g_op_eeprom_WRITEPAGE;
    SerialPrintValue("AVR_OP_WRITEPAGE op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_flash_READ_LO = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,0,7},
}};
static const OPCODE g_op_flash_READ_HI = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,0,7},
}};
static const OPCODE g_op_flash_LOADPAGE_LO = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_flash_LOADPAGE_HI = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_flash_WRITEPAGE = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
static void init_memtype_flash(AVRMEM *current_mem)
{
    int opnum;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_flash()\r\n");
    
//    current_mem->buf = (unsigned char*)osal_mem_alloc(128);
//    current_mem->tags = (unsigned char*)osal_mem_alloc(128);
    memset(g_flash_buf, 0, sizeof(g_flash_buf));
    //memcpy(g_flash_buf, (void*)g_blink_bin, sizeof(g_blink_bin));
    //memcpy(g_flash_buf, (void*)g_blink1_bin, sizeof(g_blink1_bin));
    current_mem->buf = g_flash_buf;
    current_mem->tags = NULL;
    
    SerialPrintValue("buf = 0x",(uint16)current_mem->buf, 16);
    SerialPrintValue("tags = 0x",(uint16)current_mem->tags, 16);
    
    strncpy(current_mem->desc, "flash", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->paged = 1;

    current_mem->size = 32768;

    current_mem->page_size = 128;

    current_mem->num_pages = 256;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    current_mem->readback[0] = 0xff;
    current_mem->readback[1] = 0xff;

    current_mem->mode = 0x41;

    current_mem->delay = 6;

    current_mem->blocksize = 128;

    current_mem->readsize = 256;

    opnum = AVR_OP_READ_LO;
    op = (OPCODE*)&g_op_flash_READ_LO;
    SerialPrintValue("AVR_OP_READ_LO op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_READ_HI;
    op = (OPCODE*)&g_op_flash_READ_HI;
    SerialPrintValue("AVR_OP_READ_HI op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_LO;
    op = (OPCODE*)&g_op_flash_LOADPAGE_LO;
    SerialPrintValue("AVR_OP_LOADPAGE_LO op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_HI;
    op = (OPCODE*)&g_op_flash_LOADPAGE_HI;
    SerialPrintValue("AVR_OP_LOADPAGE_HI op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITEPAGE;
    op = (OPCODE*)&g_op_flash_WRITEPAGE;
    SerialPrintValue("AVR_OP_WRITEPAGE op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_lfuse_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_lfuse_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static void init_memtype_lfuse(AVRMEM *current_mem)
{
    int opnum;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_lfuse()\r\n");
    strncpy(current_mem->desc, "lfuse", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    op = (OPCODE*)&g_op_lfuse_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    op = (OPCODE*)&g_op_lfuse_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_hfuse_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_hfuse_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static void init_memtype_hfuse(AVRMEM *current_mem)
{
    int opnum;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_hfuse()\r\n");
    strncpy(current_mem->desc, "hfuse", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    op = (OPCODE*)&g_op_hfuse_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    op = (OPCODE*)&g_op_hfuse_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_efuse_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_efuse_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,1,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static void init_memtype_efuse(AVRMEM *current_mem)
{
    int opnum;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_efuse()\r\n");
    strncpy(current_mem->desc, "efuse", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    op = (OPCODE*)&g_op_efuse_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    op = (OPCODE*)&g_op_efuse_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_lock_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
}};
static const OPCODE g_op_lock_WRITE = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{1,1,6},{1,1,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,1,5},{1,1,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
static void init_memtype_lock(AVRMEM *current_mem)
{
    int opnum;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_lock()\r\n");
    strncpy(current_mem->desc, "lock", AVR_MEMDESCLEN);
    SerialPrintString(current_mem->desc);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    opnum = AVR_OP_READ;
    op = (OPCODE*)&g_op_lock_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITE;
    op = (OPCODE*)&g_op_lock_WRITE;
    SerialPrintValue("AVR_OP_WRITE op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_calibration_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,0,6},{1,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,1,4},{1,1,5},{1,0,6},{1,0,7},
}};
static void init_memtype_calibration(AVRMEM *current_mem)
{
    int opnum;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_calibration()\r\n");
    strncpy(current_mem->desc, "calibration", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 1;

    opnum = AVR_OP_READ;
    op = (OPCODE*)&g_op_calibration_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
}

static const OPCODE g_op_signature_READ = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,1,4},{1,1,5},{1,0,6},{1,0,7},
}};
static void init_memtype_signature(AVRMEM *current_mem)
{
    int opnum;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_signature()\r\n");
    strncpy(current_mem->desc, "signature", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->size = 3;

    opnum = AVR_OP_READ;
    op = (OPCODE*)&g_op_signature_READ;
    SerialPrintValue("AVR_OP_READ op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
}

void initAvrPart_m328p(AVRPART *current_part, char* partId)
{
    SerialPrintValue("Enter initAvrPart_m328p() current_part = 0x",(uint16)current_part, 16);
    
    current_part->reset_disposition = RESET_DEDICATED;
    current_part->retry_pulse = PIN_AVR_SCK;
    current_part->flags = AVRPART_SERIALOK | AVRPART_ENABLEPAGEPROGRAMMING;
    memset(current_part->signature, 0xFF, 3);
    current_part->ctl_stack_type = CTL_STACK_NONE;
    current_part->ocdrev = -1;
    current_part->mem = lcreat(&g_memList, 8);
    SerialPrintValue("mem = 0x", (uint16)current_part->mem, 16);

    int nbytes = 0;

    if (strcmp(partId, "m328p") == 0)
    {
        strncpy(current_part->id, "m328p", AVR_IDLEN);
        current_part->id[AVR_IDLEN - 1] = 0;

        strncpy(current_part->desc, "ATmega328P", AVR_DESCLEN);
        current_part->desc[AVR_DESCLEN - 1] = 0;

        current_part->signature[0] = 0x1e;
        current_part->signature[1] = 0x95;
        current_part->signature[2] = 0x0F;
    }
    else
    {
        strncpy(current_part->id, "m328", AVR_IDLEN);
        current_part->id[AVR_IDLEN - 1] = 0;

        strncpy(current_part->desc, "ATmega328", AVR_DESCLEN);
        current_part->desc[AVR_DESCLEN - 1] = 0;

        current_part->signature[0] = 0x1e;
        current_part->signature[1] = 0x95;
        current_part->signature[2] = 0x14;
    }
    
    current_part->ocdrev = 1;

    current_part->flags |= AVRPART_HAS_DW;

    current_part->flash_instr[nbytes++] = 0xB6;
    current_part->flash_instr[nbytes++] = 0x01;
    current_part->flash_instr[nbytes++] = 0x11;

    memcpy(current_part->eeprom_instr, (void *)g_eeprom_instr, EEPROM_INSTR_SIZE);

    current_part->stk500_devcode = 0x86;

    current_part->pagel = 0xd7;

    current_part->bs2 = 0xc2;

    current_part->chip_erase_delay = 9000;

    int opnum = AVR_OP_PGM_ENABLE;
    SerialPrintString("777............\r\n");
    OPCODE *op = (OPCODE*)&g_op_part_PGM_ENABLE;
    current_part->op[opnum] = op;
    ////////////////////////////////////////////////////////////////////////////
    opnum = AVR_OP_CHIP_ERASE;
    op = (OPCODE*)&g_op_part_CHIP_ERASE;
    SerialPrintValue("AVR_OP_CHIP_ERASE op = 0x",(uint16)op, 16);
    
    current_part->op[opnum] = op;

    current_part->timeout = 200;

    current_part->stabdelay = 100;

    current_part->cmdexedelay = 25;

    current_part->synchloops = 32;

    current_part->bytedelay = 0;

    current_part->pollindex = 3;

    current_part->pollvalue = 0x53;

    current_part->predelay = 1;

    current_part->postdelay = 1;

    current_part->pollmethod = 1;

    memcpy(current_part->controlstack, (void *)g_controlstack, CTL_STACK_SIZE);

    current_part->hventerstabdelay = 100;

    current_part->progmodedelay = 0;

    current_part->latchcycles = 5;

    current_part->togglevtg = 1;

    current_part->poweroffdelay = 15;

    current_part->resetdelayms = 1;

    current_part->resetdelayus = 0;

    current_part->hvleavestabdelay = 15;

    current_part->resetdelay = 15;

    current_part->chiperasepulsewidth = 0;

    current_part->chiperasepolltimeout = 10;

    current_part->programfusepulsewidth = 0;

    current_part->programfusepolltimeout = 5;

    current_part->programlockpulsewidth = 0;

    current_part->programlockpolltimeout = 5;

    /////////////////////////////////////////////////////////////////////
    AVRMEM *current_mem;
#if 0
    //AVRMEM *current_mem = avr_new_memtype();
    current_mem = &g_mem[0];
    init_memtype_eeprom(current_mem);
    ladd(current_part->mem, current_mem);
#endif
    /////////////////////////////////////////////////////////////////////

    SerialPrintString("888............\r\n");
//    current_mem = &g_mem[1];
    current_mem = &g_mem1;
    init_memtype_flash(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
#if 0
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[2];
    init_memtype_lfuse(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[3];
    init_memtype_hfuse(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[4];
    init_memtype_efuse(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[5];
    init_memtype_lock(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[6];
    init_memtype_calibration(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
    //current_mem = avr_new_memtype();
    current_mem = &g_mem[7];
    init_memtype_signature(current_mem);
    ladd(current_part->mem, current_mem);
    //////////////////////////////////////////////////////////////////////////////
#endif
    SerialPrintString("9............\r\n");
}
/*********************************************************************
*********************************************************************/
