/*********************************************************************
 * INCLUDES
 */
#include "ac_cfg.h"

//#include <stdio.h>
#include <stdlib.h>
//#include <errno.h>
//#include <fcntl.h>
//#include <limits.h>
#include <string.h>
//#include <time.h>
//#include <unistd.h>
//#include <ctype.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <sys/time.h>
#include "OSAL.h"
#include "avr.h"
//#include "config.h"
//#include "confwin.h"
#include "fileio.h"
#include "lists.h"
//#include "par.h"
#include "pindefs.h"
//#include "term.h"
////#include "safemode.h"
#include "update.h"
#include "pgm_type.h"

#include "arduino.h"
/* Get VERSION from ac_cfg.h */
//char * version      = VERSION;
#include "..\SerialApp.h"
#include "..\elara_file_common.h"
#include "main_common.h"

#define fprintf(s, ...)
/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * EXTERNAL FUNCTIONS
 */


/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
unsigned char const g_controlstack_m644p[CTL_STACK_SIZE] = {
    0x0E, 0x1E, 0x0F, 0x1F, 0x2E, 0x3E, 0x2F, 0x3F, 
    0x4E, 0x5E, 0x4F, 0x5F, 0x6E, 0x7E, 0x6F, 0x7F,
    0x66, 0x76, 0x67, 0x77, 0x6A, 0x7A, 0x6B, 0x7B, 
    0xBE, 0xFD, 0x00, 0x01, 0x00, 0x00, 0x00, 0x02
};
const OPCODE g_op_part_PGM_ENABLE_m644p = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {1,1,0},{1,1,1},{1,0,2},{1,0,3},{1,1,4},{1,0,5},{1,1,6},{1,0,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};
const OPCODE g_op_part_CHIP_ERASE_m644p = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{1,0,5},{1,0,6},{1,1,7},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,1,7},
}};

const OPCODE g_op_flash_READ_LO_m644p = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{2,0,14},{2,0,15},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,1,5},{1,0,6},{1,0,7},
}};
const OPCODE g_op_flash_READ_HI_m644p = {{
    {4,0,0},{4,0,1},{4,0,2},{4,0,3},{4,0,4},{4,0,5},{4,0,6},{4,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{2,0,14},{2,0,15},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,1,5},{1,0,6},{1,0,7},
}};
const OPCODE g_op_flash_LOADPAGE_LO_m644p = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,0,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
const OPCODE g_op_flash_LOADPAGE_HI_m644p = {{
    {3,0,0},{3,0,1},{3,0,2},{3,0,3},{3,0,4},{3,0,5},{3,0,6},{3,0,7},
    {2,0,0},{2,0,1},{2,0,2},{2,0,3},{2,0,4},{2,0,5},{2,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{1,0,6},{1,0,7},
    {1,0,0},{1,0,1},{1,0,2},{1,1,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
const OPCODE g_op_flash_WRITEPAGE_m644p = {{
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{0,0,7},
    {0,0,0},{0,0,1},{0,0,2},{0,0,3},{0,0,4},{0,0,5},{0,0,6},{2,0,7},
    {2,0,8},{2,0,9},{2,0,10},{2,0,11},{2,0,12},{2,0,13},{2,0,14},{2,0,15},
    {1,0,0},{1,0,1},{1,1,2},{1,1,3},{1,0,4},{1,0,5},{1,1,6},{1,0,7},
}};
/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void init_memtype_flash(AVRMEM *current_mem)
{
    int opnum;
    OPCODE *op;
    SerialPrintString("Enter init_memtype_flash()\r\n");
    
//    current_mem->buf = (unsigned char*)osal_mem_alloc(128);
//    current_mem->tags = (unsigned char*)osal_mem_alloc(128);
    memset(g_flash_buf, 0, sizeof(g_flash_buf));
    //memcpy(g_flash_buf, (void*)g_blink_bin, sizeof(g_blink_bin));
    //memcpy(g_flash_buf, (void*)g_blink1_bin, sizeof(g_blink1_bin));
    current_mem->buf = g_flash_buf;
    current_mem->tags = NULL;
    
    SerialPrintValue("buf = 0x",(uint16)current_mem->buf, 16);
    SerialPrintValue("tags = 0x",(uint16)current_mem->tags, 16);
    
    strncpy(current_mem->desc, "flash", AVR_MEMDESCLEN);
    current_mem->desc[AVR_MEMDESCLEN - 1] = 0;
    SerialPrintString(current_mem->desc);
    SerialPrintString("\r\n");

    current_mem->paged = 1;

    current_mem->size = 65536;

    current_mem->page_size = 256;

    current_mem->num_pages = 256;

    current_mem->min_write_delay = 4500;

    current_mem->max_write_delay = 4500;

    current_mem->readback[0] = 0xff;
    current_mem->readback[1] = 0xff;

    current_mem->mode = 0x21;

    current_mem->delay = 6;

    current_mem->blocksize = 256;

    current_mem->readsize = 256;

    opnum = AVR_OP_READ_LO;
    op = (OPCODE*)&g_op_flash_READ_LO_m644p;
    SerialPrintValue("AVR_OP_READ_LO op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_READ_HI;
    op = (OPCODE*)&g_op_flash_READ_HI_m644p;
    SerialPrintValue("AVR_OP_READ_HI op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_LO;
    op = (OPCODE*)&g_op_flash_LOADPAGE_LO_m644p;
    SerialPrintValue("AVR_OP_LOADPAGE_LO op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_LOADPAGE_HI;
    op = (OPCODE*)&g_op_flash_LOADPAGE_HI_m644p;
    SerialPrintValue("AVR_OP_LOADPAGE_HI op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
    //.........................................
    opnum = AVR_OP_WRITEPAGE;
    op = (OPCODE*)&g_op_flash_WRITEPAGE_m644p;
    SerialPrintValue("AVR_OP_WRITEPAGE op = 0x",(uint16)op, 16);
    current_mem->op[opnum] = op;
}

void initAvrPart_m644p(AVRPART *current_part, char* partId)
{
    SerialPrintValue("Enter initAvrPart_m644p() current_part = 0x",(uint16)current_part, 16);
    
    current_part->reset_disposition = RESET_DEDICATED;
    current_part->retry_pulse = PIN_AVR_SCK;
    current_part->flags = AVRPART_SERIALOK | AVRPART_ENABLEPAGEPROGRAMMING;
    memset(current_part->signature, 0xFF, 3);
    current_part->ctl_stack_type = CTL_STACK_NONE;
    current_part->ocdrev = -1;
    current_part->mem = lcreat(&g_memList, 8);
    SerialPrintValue("mem = 0x", (uint16)current_part->mem, 16);

    if (strcmp(partId, "m644p") == 0)
    {
        strncpy(current_part->id, "m644p", AVR_IDLEN);
        current_part->id[AVR_IDLEN - 1] = 0;

        strncpy(current_part->desc, "ATmega644P", AVR_DESCLEN);
        current_part->desc[AVR_DESCLEN - 1] = 0;

        current_part->signature[0] = 0x1e;
        current_part->signature[1] = 0x96;
        current_part->signature[2] = 0x0a;
    }
    else
    {
        strncpy(current_part->id, "m644", AVR_IDLEN);
        current_part->id[AVR_IDLEN - 1] = 0;

        strncpy(current_part->desc, "ATmega644", AVR_DESCLEN);
        current_part->desc[AVR_DESCLEN - 1] = 0;

        current_part->signature[0] = 0x1e;
        current_part->signature[1] = 0x96;
        current_part->signature[2] = 0x09;
    }
    
    current_part->ocdrev = 3;

    current_part->flags |= AVRPART_HAS_JTAG;

    current_part->stk500_devcode = 0x82;

    current_part->avr910_devcode = 0x74;

    current_part->pagel = 0xd7;

    current_part->bs2 = 0xa0;

    current_part->chip_erase_delay = 55000;

    int opnum = AVR_OP_PGM_ENABLE;
    SerialPrintString("777............\r\n");
    OPCODE *op = (OPCODE*)&g_op_part_PGM_ENABLE_m644p;
    current_part->op[opnum] = op;
    ////////////////////////////////////////////////////////////////////////////
    opnum = AVR_OP_CHIP_ERASE;
    op = (OPCODE*)&g_op_part_CHIP_ERASE_m644p;
    SerialPrintValue("AVR_OP_CHIP_ERASE op = 0x",(uint16)op, 16);
    current_part->op[opnum] = op;

    current_part->timeout = 200;

    current_part->stabdelay = 100;

    current_part->cmdexedelay = 25;

    current_part->synchloops = 32;

    current_part->bytedelay = 0;

    current_part->pollindex = 3;

    current_part->pollvalue = 0x53;

    current_part->predelay = 1;

    current_part->postdelay = 1;

    current_part->pollmethod = 0;

    memcpy(current_part->controlstack, (void *)g_controlstack_m644p, CTL_STACK_SIZE);

    current_part->hventerstabdelay = 100;

    current_part->progmodedelay = 0;

    current_part->latchcycles = 6;

    current_part->togglevtg = 0;

    current_part->poweroffdelay = 0;

    current_part->resetdelayms = 0;

    current_part->resetdelayus = 0;

    current_part->hvleavestabdelay = 15;

    current_part->chiperasepulsewidth = 0;

    current_part->chiperasepolltimeout = 10;

    current_part->programfusepulsewidth = 0;

    current_part->programfusepolltimeout = 5;

    current_part->programlockpulsewidth = 0;

    current_part->programlockpolltimeout = 5;

    current_part->idr = 0x31;

    current_part->spmcr = 0x57;

    current_part->flags &= ~AVRPART_ALLOWFULLPAGEBITSTREAM;

    AVRMEM *current_mem;
    current_mem = &g_mem1;
    init_memtype_flash(current_mem);
    ladd(current_part->mem, current_mem);
}
/*********************************************************************
*********************************************************************/
