#ifndef MAIN_M1284P_H
#define MAIN_M1284P_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */

#include "hal_types.h"

/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * FUNCTIONS
 */
extern void initAvrPart_m1284p(AVRPART *current_part, char* partId);
/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif
