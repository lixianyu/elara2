/**************************************************************************************************
  Filename:       keyfobdemo.c
  Revised:        $Date: 2013-08-15 15:28:40 -0700 (Thu, 15 Aug 2013) $
  Revision:       $Revision: 34986 $

  Description:    Key Fob Demo Application.

  Copyright 2009 - 2013 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED 揂S IS?WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <string.h>
#include "bcomdef.h"
#include "OSAL.h"
#include "osal_snv.h"
#include "OSAL_PwrMgr.h"
#include "OnBoard.h"
#include "hal_adc.h"
#include "hal_led.h"
#include "hal_key.h"
#include "gatt.h"
#include "hci.h"
#include "gapgattserver.h"
#include "gattservapp.h"
#include "gatt_profile_uuid.h"

#if defined ( PLUS_BROADCASTER )
#include "peripheralBroadcaster.h"
#else
#include "peripheral.h"
#endif
#if defined (ELARA_BOND)
#include "gapbondmgr.h"
#else
#include "gap.h"
#endif
#include "hal_uart.h"
#if defined(GLIESE)
#include "gliese.h"
#endif
#include "simpleGATTprofile.h"
#include "devinfoservice.h"

#include "battservice.h"

#include "simplekeys.h"
#include "ccservice.h"
#include "keyfobdemo.h"

#if defined(FEATURE_OAD)
#include "oad.h"
#include "oad_target.h"
#endif
#include "SerialApp.h"
#include "stk500_private.h"
#if defined(STK500_ASYNCHRONOUS)
#include "m328p.h"
#endif
#include "elara_file_common.h"
#include "elara_led.h"
#include "md_profile.h"
#include "md_uart_profile.h"
#include "main_common.h"

/*********************************************************************
 * MACROS
 */
/* Ative delay: 125 cycles ~1 msec */
#define KFD_HAL_DELAY(n) st( { volatile uint32 i; for (i=0; i<(n); i++) { }; } )

/*********************************************************************
 * CONSTANTS
 */

// Delay between power-up and starting advertising (in ms)
#define STARTDELAY                            500

// How often to check battery voltage (in ms)
#define BATTERY_CHECK_PERIOD                  15000

//GAP Peripheral Role desired connection parameters

// Use limited discoverable mode to advertise for 30.72s, and then stop, or
// use general discoverable mode to advertise indefinitely
//#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_LIMITED
#define DEFAULT_DISCOVERABLE_MODE             GAP_ADTYPE_FLAGS_GENERAL

#if 1
/* Apple Connnection Params Limits
 1. Interval Max * (Slave Latency + 1) ≤ 2 seconds；
 2. Interval Min ≥ 20 ms；
 3. Interval Min + 20 ms ≤ Interval Max；
 4. Slave Latency ≤ 4；
 5. connSupervisionTimeout ≤ 6 seconds
 6. Interval Max * (Slave Latency + 1) * 3 < connSupervisionTimeout
 */
// Minimum connection interval (units of 1.25ms, 80=100ms) if automatic parameter update request is enabled
// 1500 = 1875ms
// 16 = 20ms
// 30 = 37.5ms
// 33 = 41.25ms
// 96 = 120ms
// 176 = 220ms
// 189 = 236.25ms
// 302 = 377.5ms
// 303 = 378.75ms
//#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     303
#if 0
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     16
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     32
#else
// 303 = 378.75ms
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     303
// 319 = 398.75ms
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     319
#endif
// Maximum connection interval (units of 1.25ms, 800=1000ms) if automatic parameter update request is enabled
// 1522 = 1902.5ms
// 190 = 237.5ms
// 192 = 240ms --> On iOS7.0.4, this is the max value!!!
// 200 = 250ms
// 210 = 262.5ms
// 318 = 397.5ms
// 319 = 398.75ms
//#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     97
//#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     33

// Slave latency to use if automatic parameter update request is enabled
#define DEFAULT_DESIRED_SLAVE_LATENCY         4

// Supervision timeout value (units of 10ms, 1000=10s) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_CONN_TIMEOUT          600
#else
// Minimum connection interval (units of 1.25ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_MIN_CONN_INTERVAL     80

// Maximum connection interval (units of 1.25ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_MAX_CONN_INTERVAL     800

// Slave latency to use if automatic parameter update request is enabled
#define DEFAULT_DESIRED_SLAVE_LATENCY         0

// Supervision timeout value (units of 10ms) if automatic parameter update request is enabled
#define DEFAULT_DESIRED_CONN_TIMEOUT          1000
#endif
// Whether to enable automatic parameter update request when a connection is formed
#define DEFAULT_ENABLE_UPDATE_REQUEST         FALSE

// Connection Pause Peripheral time value (in seconds)
#define DEFAULT_CONN_PAUSE_PERIPHERAL         3

#define iBeacon

//Minimum advertising interval, when in General discoverable mode (n * 0.625 mSec)
// 1600 * 0.625 = 1000ms
// 3200 * 0.625 = 2000ms
// 4800 * 0.625 = 3000ms
// 8000 * 0.625 = 5000ms
// 9600 * 0.625 = 6000ms
// 16000 * 0.625 = 10000ms
// 12191 * 0.625 = 7619.375ms

#if 0
//#define DEFAULT_DISC_ADV_INT_MIN         3200 //1600
//#define DEFAULT_DISC_ADV_INT_MID         3200  //8000
//#define DEFAULT_DISC_ADV_INT_MAX         3200 //16000
static uint16 gAdvIntervalMin = 1600;
static uint16 gAdvIntervalMax = 1600;

//static uint16 gAdvIntervalMin = 1600;
//static uint16 gAdvIntervalMax = 4800;

//static uint16 gAdvIntervalMin = 1600;
//static uint16 gAdvIntervalMax = 1600;

#else
#define DEFAULT_DISC_ADV_INT_MIN         1281
//#define DEFAULT_DISC_ADV_INT_MID         8000
#define DEFAULT_DISC_ADV_INT_MAX         1281
//static uint16 gAdvIntervalMin = 1600;
//static uint16 gAdvIntervalMax = 12191;
#endif

//#define DEFAULT_LIM_DISC_ADV_INT_MIN         DEFAULT_DISC_ADV_INT_MIN
//#define DEFAULT_LIM_DISC_ADV_INT_MID         DEFAULT_DISC_ADV_INT_MID
//#define DEFAULT_LIM_DISC_ADV_INT_MAX         DEFAULT_DISC_ADV_INT_MAX

//#define DEFAULT_GEN_DISC_ADV_INT_MIN         DEFAULT_DISC_ADV_INT_MIN
//#define DEFAULT_GEN_DISC_ADV_INT_MID         DEFAULT_DISC_ADV_INT_MID
//#define DEFAULT_GEN_DISC_ADV_INT_MAX         DEFAULT_DISC_ADV_INT_MAX


// Company Identifier: Texas Instruments Inc. (13)
#define TI_COMPANY_ID                         0x000D

#define INVALID_CONNHANDLE                    0xFFFF

#if defined ( PLUS_BROADCASTER )
#define ADV_IN_CONN_WAIT                    500 // delay 500 ms
#endif
#if defined (ELARA_BOND)
// Default passcode
static uint32 gDefaultPasscode = 2015;
#endif

/*********************************************************************
 * TYPEDEFS
 */
typedef enum elara_state
{
    FSM_IDLE = 0,
    FSM_getsync,
    FSM_GET_MAJOR,
    FSM_GET_MINOR,

} ELARA_STATE;

typedef enum
{
    m328p_16M_5V = 0,//Upload speed is 115200
    m328p_8M_3_3V,   //Upload speed is 57600

    m644p_16M_5V,    //Upload speed is 115200
    m644p_8M_3_3V,   //Upload speed is 57600

    m1284p_16M_5V,   //Upload speed is 115200
    m1284p_8M_3_3V,  //Upload speed is 57600

    m32u4_16M,       //Upload speed is 57600
    m128rfa1_16M,     //Upload speed is 57600

    m128rfr2,
    m64rfr2,
    m2560,
    m2561,

    m328_16M_5V,     //Upload speed is 115200
    m328_8M_3_3V,    //Upload speed is 57600
    m644_16M_5V,     //Upload speed is 115200
    m644_8M_3_3V,    //Upload speed is 57600
    m1284_16M_5V,    //Upload speed is 115200
    m1284_8M_3_3V,   //Upload speed is 57600


} t_part_id;

typedef enum
{
    FlashType_CC2541 = 0,
    FlashType_w25q,
} FlashType;

typedef enum
{
    UPDATE_AVR_IDLE = 0,
    UPDATE_AVR_CONN_PARAM_UPDATE,
    UPDATE_AVR_INIT_ELARA_SNV,
    UPDATE_AVR_INIT_AVR,
    UPDATE_AVR_STATE_2,
    UPDATE_AVR_STATE_3,
    UPDATE_AVR_STATE_4,
    UPDATE_AVR_STATE_5,
    UPDATE_AVR_STATE_6,
} UpdateAvrState;
/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
uint8 keyfobapp_TaskID;   // Task ID for internal task/event processing

static gaprole_States_t gapProfileState = GAPROLE_INIT;
#if defined(STK500_ASYNCHRONOUS)
static ELARA_STATE gFSM = FSM_IDLE;
#endif
static UpdateAvrState gUAS = UPDATE_AVR_IDLE;

// GAP - SCAN RSP data (max size = 31 bytes)
// GAP Profile - Name attribute for SCAN RSP data
static uint8 elaraScanResponseData[] =
{
    0x0C,   // length of this data
    GAP_ADTYPE_LOCAL_NAME_COMPLETE,
    0x6D,   // 'm'
    0x43,   // 'C'
    0x6F,   // 'o'
    0x6F,   // 'o'
    0x6B,   // 'k' 
    0x69,   // 'i'
    0x65,   // 'e'
    1, 2, 3, 4,

    0x02,   // length of this data
    GAP_ADTYPE_POWER_LEVEL, //TX Power Level
    0       // 0dBm
};


// GAP - Advertisement data (max size = 31 bytes, though this is
// best kept short to conserve power while advertisting)


/* For iBeacon adver data:
# Actual Advertising Data Starts Here
02 01 1a
1a ff 4c 00 02 15 # Apple's static prefix to the advertising data -- this is always the same
e2 c5 6d b5 df fb 48 d2 b0 60 d0 f5 a7 10 96 e0 # iBeacon profileUUID
00 00 # major (LSB first)
00 00 # minor (LSB first)
c5 # The 2's complement of the calibrated Tx Power
 */

static uint8 advertDataBeacon_OLD[] =
{
    0x02,   // length of first data structure (2 bytes excluding length byte)
    GAP_ADTYPE_FLAGS,   // AD Type = Flags
    //0x1a,
    GAP_ADTYPE_FLAGS_GENERAL | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

    // service UUID, to notify central devices what services are included
    // in this peripheral
    0x1B,   // length of second data structure (7 bytes excluding length byte)
    0xff, 0x4c, 0x00, 0x02, 0x15,
    0xFD, 0xA5, 0x06, 0x93, 0xA4, 0xE2, 0x4F, 0xB1, 0xAF, 0xCF, 0xC6, 0xEB, 0x07, 0x64, 0x78, 0x25,
    //0xe2, 0xc5, 0x6d, 0xb5, 0xdf, 0xfb, 0x48, 0xd2, 0xb0, 0x60, 0xd0, 0xf5, 0xa7, 0x10, 0x96, 0xe0,
#if 0
    0x00, 0x0a,
    0x00, 0x07,
#elif 1
    0x27, 0x11,
    0xE9, 0xDD,
#else
    0x27, 0x11, //10001
    0x09, 0xE0, //59872
#endif
    0xc5,
    0xBB,
};

static uint8 advertDataBeacon[] = 
{ 
  0x02,   // length of this data
  GAP_ADTYPE_FLAGS,
  GAP_ADTYPE_FLAGS_GENERAL | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

  // service UUID
  0x05,   // length of this data
  GAP_ADTYPE_16BIT_MORE,
  LO_UINT16( 0xFFF0 ),
  HI_UINT16( 0xFFF0 ),
  LO_UINT16( 0xFFE0 ),
  HI_UINT16( 0xFFE0 ),
};

static CONST uint8 advertDataConst[] =
{
    0x02,   // length of first data structure (2 bytes excluding length byte)
    GAP_ADTYPE_FLAGS,   // AD Type = Flags
    //0x1a,
    GAP_ADTYPE_FLAGS_GENERAL | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

    // service UUID, to notify central devices what services are included
    // in this peripheral
    0x1B,   // length of second data structure (7 bytes excluding length byte)
    0xff, 0x4c, 0x00, 0x02, 0x15,
    0xFD, 0xA5, 0x06, 0x93, 0xA4, 0xE2, 0x4F, 0xB1, 0xAF, 0xCF, 0xC6, 0xEB, 0x07, 0x64, 0x78, 0x25,
    //0xe2, 0xc5, 0x6d, 0xb5, 0xdf, 0xfb, 0x48, 0xd2, 0xb0, 0x60, 0xd0, 0xf5, 0xa7, 0x10, 0x96, 0xe0,
#if 0
    0x00, 0x0a,
    0x00, 0x07,
#elif 0
    0x27, 0x11,
    0xE9, 0xE2,
#else
    0x27, 0x11, //10001
    0xE9, 0xE0, //59872
#endif
    0xc5,
    0xBB,
};
#if 0
static uint8 advertDataSmall[] =
{
    0x02,   // length of first data structure (2 bytes excluding length byte)
    GAP_ADTYPE_FLAGS,   // AD Type = Flags
    DEFAULT_DISCOVERABLE_MODE | GAP_ADTYPE_FLAGS_BREDR_NOT_SUPPORTED,

    0x0B,
    GAP_ADTYPE_16BIT_MORE,
    0x01, 0x02, 0x03, 0x04, 0x05, 0x06, // GAPROLE_BD_ADDR
    0x00, 0x64, // Battery value
    0x50, 0x1D, // Temperature
};
#endif

#if defined(HAL_IMAGE_A)
// GAP GATT Attributes
const static uint8 elara_attDeviceName[GAP_DEVICE_NAME_LEN] = "ElaraA";
#elif defined(HAL_IMAGE_B)
const static uint8 elara_attDeviceName[GAP_DEVICE_NAME_LEN] = "ElaraB";
#else
static uint8 elara_attDeviceName[GAP_DEVICE_NAME_LEN] = "mCookie";
#endif

//static uint8 gBatteryValue = 100;
static uint8 gWriteDone;
uint8 gManualTerminate = 0;
static uint32 gElaraBaud = HAL_UART_BR_115200;
static uint32 gCurPartID = m328p_16M_5V;
/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void keyfobapp_ProcessOSALMsg( osal_event_hdr_t *pMsg );
static void keyfobapp_HandleKeys( uint8 shift, uint8 keys );
static void peripheralStateNotificationCB( gaprole_States_t newState );
static void phobosRateBattCB(uint8 event);
static void ccServiceChangeCB( uint8 paramID );
#if defined (ELARA_BOND)
static uint8 gPairStatus = 0; /*用来管理当前的状态，如果密码不正确，立即取消连接，0表示未配对，1表示已配对*/
static void ProcessPasscodeCB(uint8 *deviceAddr, uint16 connectionHandle, uint8 uiInputs, uint8 uiOutputs );
static void ProcessPairStateCB( uint16 connHandle, uint8 state, uint8 status );
#endif
#if defined(GLIESE)
static void glieseServiceChangeCB( uint8 paramID );
#endif
static void simpleProfileChangeCB( uint8 paramID );
static void md_ProfileChangeCB( uint8 paramID );
static void md_UARTProfileChangeCB( uint8 paramID );
#if defined(STK500_ASYNCHRONOUS)
static int elara_fsm(void);
#endif
static void processAVRevent(void);
static void UpdateAVRevent(void);
static void resolve_command(void);
bool parse_at_command(uint8 *pBuffer, uint16 length);
/*********************************************************************
 * PROFILE CALLBACKS
 */

// GAP Role Callbacks
static const gapRolesCBs_t keyFob_PeripheralCBs =
{
    peripheralStateNotificationCB,  // Profile State Change Callbacks
    NULL                // When a valid RSSI is read from controller
};

#if defined (ELARA_BOND)
// GAP Bond Manager Callbacks
static gapBondCBs_t keyFob_BondMgrCBs =
{
    ProcessPasscodeCB,                     // Passcode callback
    ProcessPairStateCB                     // Pairing / Bonding state Callback
};
#endif

static const ccCBs_t keyfob_ccCBs =
{
    ccServiceChangeCB,               // Charactersitic value change callback
};
#if defined(GLIESE)
static glieseProfileCBs_t keyfob_GlieseCBs =
{
    glieseServiceChangeCB,               // Charactersitic value change callback
};
#endif
// Simple GATT Profile Callbacks
static const simpleProfileCBs_t simpleBLEPeripheral_SimpleProfileCBs =
{
    simpleProfileChangeCB    // Charactersitic value change callback
};

static const mdProfileCBs_t MDProfileCB =
{
    md_ProfileChangeCB
};
static const mdUartProfileCBs_t MDUARTProfileCB =
{
    md_UARTProfileChangeCB
};
/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      KeyFobApp_Init
 *
 * @brief   Initialization function for the Key Fob App Task.
 *          This is called during initialization and should contain
 *          any application specific initialization (ie. hardware
 *          initialization/setup, table initialization, power up
 *          notificaiton ... ).
 *
 * @param   task_id - the ID assigned by OSAL.  This ID should be
 *                    used to send messages and set timers.
 *
 * @return  none
 */
void KeyFobApp_Init( uint8 task_id )
{
    keyfobapp_TaskID = task_id;
    uint8 ret = 1;

    // Setup the GAP
    VOID GAP_SetParamValue( TGAP_CONN_PAUSE_PERIPHERAL, DEFAULT_CONN_PAUSE_PERIPHERAL );

#if defined(GLIESE)
    uint8 beacon[GLIESEPROFILE_CHAR2_LEN] = {0};

    ret = osal_snv_read(BLE_NVID_iBeacon_SET, GLIESEPROFILE_CHAR2_LEN, beacon);
    if (ret == SUCCESS)// set iBeacon UUID
    {
        osal_memcpy(advertDataBeacon + 9, beacon, 16);
    }
    GlieseProfile_SetParameter(GLIESEPROFILE_CHAR2, GLIESEPROFILE_CHAR2_LEN, advertDataBeacon + 9);

    uint8 beacon1[GLIESEPROFILE_CHAR1_LEN - 4] = {0};
    ret = osal_snv_read(BLE_NVID_iBeacon_SET1, GLIESEPROFILE_CHAR1_LEN - 4, beacon1);
    if (ret == SUCCESS)
    {
        // set major
        advertDataBeacon[25] = beacon1[0];
        advertDataBeacon[26] = beacon1[1];

        // set minor
        advertDataBeacon[27] = beacon1[2];
        advertDataBeacon[28] = beacon1[3];
        GlieseProfile_SetParameter(GLIESEPROFILE_CHAR1, GLIESEPROFILE_CHAR1_LEN - 4, beacon1);
        // set The 2's complement of the calibrated Tx Power
        advertDataBeacon[29] = beacon1[4];

        //Tx Power
        uint8 txPower = HCI_EXT_TX_POWER_0_DBM;
        switch (beacon1[5])
        {
        case 0:
            txPower = HCI_EXT_TX_POWER_MINUS_23_DBM;
            break;

        case 1:
            txPower = HCI_EXT_TX_POWER_MINUS_6_DBM;
            break;

        case 2:
            txPower = HCI_EXT_TX_POWER_0_DBM;
            break;

        case 3:
            txPower = HCI_EXT_TX_POWER_0_DBM;
            break;
        }
        HCI_EXT_SetTxPowerCmd( txPower );

        //Advertising Freq (1-100) Unit: 100ms
        if (beacon1[6] != 0)
        {
            gAdvIntervalMin = (uint16)((float)beacon1[6] * (float)100 / (float)0.625);
            if (gAdvIntervalMin < 1600)
            {
                gAdvIntervalMin = 1600;
            }
            gAdvIntervalMax = gAdvIntervalMin;
        }
    }
#endif
    // Set fast advertising interval for user-initiated connections
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, DEFAULT_DISC_ADV_INT_MIN );
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, DEFAULT_DISC_ADV_INT_MIN );
    GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 10000 );

    // Setup the GAP Peripheral Role Profile
    {
        // For the CC2540DK-MINI keyfob, device doesn't start advertising until button is pressed
        uint8 initial_advertising_enable = FALSE;

        // By setting this to zero, the device will go into the waiting state after
        // being discoverable for 30.72 second, and will not being advertising again
        // until the enabler is set back to TRUE
        uint16 gapRole_AdvertOffTime = 0;

        uint8 enable_update_request = DEFAULT_ENABLE_UPDATE_REQUEST;
        uint16 desired_min_interval = DEFAULT_DESIRED_MIN_CONN_INTERVAL;
        uint16 desired_max_interval = DEFAULT_DESIRED_MAX_CONN_INTERVAL;
        uint16 desired_slave_latency = DEFAULT_DESIRED_SLAVE_LATENCY;
        uint16 desired_conn_timeout = DEFAULT_DESIRED_CONN_TIMEOUT;

        // Set the GAP Role Parameters
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &initial_advertising_enable );
        GAPRole_SetParameter( GAPROLE_ADVERT_OFF_TIME, sizeof( uint16 ), &gapRole_AdvertOffTime );

        GAPRole_SetParameter( GAPROLE_SCAN_RSP_DATA, sizeof ( elaraScanResponseData ), (void *)elaraScanResponseData );
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon_OLD ), (void *)advertDataBeacon_OLD );

        GAPRole_SetParameter( GAPROLE_PARAM_UPDATE_ENABLE, sizeof( uint8 ), &enable_update_request );
        GAPRole_SetParameter( GAPROLE_MIN_CONN_INTERVAL, sizeof( uint16 ), &desired_min_interval );
        GAPRole_SetParameter( GAPROLE_MAX_CONN_INTERVAL, sizeof( uint16 ), &desired_max_interval );
        GAPRole_SetParameter( GAPROLE_SLAVE_LATENCY, sizeof( uint16 ), &desired_slave_latency );
        GAPRole_SetParameter( GAPROLE_TIMEOUT_MULTIPLIER, sizeof( uint16 ), &desired_conn_timeout );
    }

    // Set the GAP Attributes
    uint8 deviceNamePhobos[GAP_DEVICE_NAME_LEN] = {0};
    ret = osal_snv_read(BLE_NVID_PHOBOS_DEVICE_NAME, GAP_DEVICE_NAME_LEN, deviceNamePhobos);
    if (ret == SUCCESS)
    {
        GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, deviceNamePhobos );
    }
    else
    {
        GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, (void *)elara_attDeviceName );
    }
#if defined (ELARA_BOND)
    // Setup the GAP Bond Manager
    {
        uint32 passkey = gDefaultPasscode;
        uint8 pairMode = GAPBOND_PAIRING_MODE_WAIT_FOR_REQ;
        uint8 mitm = TRUE;
        uint8 ioCap = GAPBOND_IO_CAP_DISPLAY_ONLY;
        uint8 bonding = TRUE;
#if defined(GLIESE)
        uint8 bondSet[8];
        ret = osal_snv_read(BLE_NVID_BOND_SETTING, 8, bondSet);
        if (ret == SUCCESS)
        {
            passkey = ((uint32)bondSet[0] << 24) |
                      ((uint32)bondSet[1] << 16) |
                      ((uint32)bondSet[2] << 8) |
                      ((uint32)bondSet[3]);
            pairMode = bondSet[4];
            mitm = bondSet[5];
            ioCap = bondSet[6];
            bonding = bondSet[7];
            gDefaultPasscode = passkey;
            GlieseProfile_SetParameter(GLIESEPROFILE_CHAR10, GLIESEPROFILE_CHAR10_LEN - 6, bondSet);
        }
        else
        {
            bondSet[0] = (passkey >> 24) & 0xFF;
            bondSet[1] = (passkey >> 16) & 0xFF;
            bondSet[2] = (passkey >> 8) & 0xFF;
            bondSet[3] = passkey & 0xFF;
            bondSet[4] = pairMode;
            bondSet[5] = mitm;
            bondSet[6] = ioCap;
            bondSet[7] = bonding;
            GlieseProfile_SetParameter(GLIESEPROFILE_CHAR10, GLIESEPROFILE_CHAR10_LEN - 6, bondSet);
        }
#endif
        GAPBondMgr_SetParameter( GAPBOND_DEFAULT_PASSCODE, sizeof ( uint32 ), &passkey );
        GAPBondMgr_SetParameter( GAPBOND_PAIRING_MODE, sizeof ( uint8 ), &pairMode );
        GAPBondMgr_SetParameter( GAPBOND_MITM_PROTECTION, sizeof ( uint8 ), &mitm );
        GAPBondMgr_SetParameter( GAPBOND_IO_CAPABILITIES, sizeof ( uint8 ), &ioCap );
        GAPBondMgr_SetParameter( GAPBOND_BONDING_ENABLED, sizeof ( uint8 ), &bonding );
    }
#endif

    // Initialize GATT attributes
    GGS_AddService( GATT_ALL_SERVICES );         // GAP
    GATTServApp_AddService( GATT_ALL_SERVICES ); // GATT attributes
#if defined(FEATURE_OAD)
    VOID OADTarget_AddService(); //OAD Profile
#endif

    uint8 rc = MDProfile_AddService();
    rc = MDUARTProfile_AddService();
    //advertDataBeacon[26] = rc;
    //GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
    //SimpleProfile_AddService( GATT_ALL_SERVICES );  // Simple GATT Profile

    DevInfo_AddService();   // Device Information Service
    Batt_AddService( );     // Battery Service
    CcService_AddService();     // Connection Control Service
#if defined(GLIESE)
    GlieseProfile_AddService( GATT_ALL_SERVICES );  // Gliese Profile for Phobos
#endif
    //SK_AddService( GATT_ALL_SERVICES );         // Simple Keys Profile
    // makes sure LEDs are off
    HalLedSet( (HAL_LED_2), HAL_LED_MODE_OFF );

    // For keyfob board set GPIO pins into a power-optimized state
    // Note that there is still some leakage current from the buzzer,
    // accelerometer, LEDs, and buttons on the PCB.
    P0SEL = 0x00; // Configure Port 0 as GPIO
    P1SEL = 0x00; // Configure Port 1 as GPIO
    P2SEL = 0;    // Configure Port 2 as GPIO

    P0DIR = 0xFF;// 0: Input,  1: Output
    P1DIR = 0xFF;
    P2DIR = 0x1F;

    P0 = 0x0; // All pins on port 0 to low
    P1 = 0x0; // All pins on port 1 to low
    P2 = 0;   // All pins on port 2 to low

    P0_6 = 1; // For AVR Rst pin
    P1_4 = 1; // SPI CS pin

    // initialize the ADC for battery reads
    HalAdcInit();

    // Register for all key events - This app will handle all key events
    RegisterForKeys( keyfobapp_TaskID );

    // Register for Battery service callback;
    Batt_Register ( phobosRateBattCB );
#if defined(GLIESE)
    GlieseProfile_RegisterAppCBs(&keyfob_GlieseCBs);
#endif
    // Register callback with SimpleGATTprofile
    //SimpleProfile_RegisterAppCBs( (simpleProfileCBs_t *)&simpleBLEPeripheral_SimpleProfileCBs );
    MDProfile_RegisterAppCBs((mdProfileCBs_t *)&MDProfileCB);
    MDUARTProfile_RegisterAppCBs((mdUartProfileCBs_t *)&MDUARTProfileCB);

    CcService_RegisterAppCBs( (ccCBs_t *)&keyfob_ccCBs );
#if defined ( DC_DC_P0_7 )
    // Enable stack to toggle bypass control on TPS62730 (DC/DC converter)
    HCI_EXT_MapPmIoPortCmd( HCI_EXT_PM_IO_PORT_P0, HCI_EXT_PM_IO_PORT_PIN7 );
#endif // defined ( DC_DC_P0_7 )
    uint8 charValue6[MD_UART_TRANS_LEN] = { 1, 2, 3, 4, 5 };
    MDUARTProfile_SetParameter( MD_UART_PROFILE_TRANS, MD_UART_TRANS_LEN, charValue6 );

    // Setup a delayed profile startup
    osal_start_timerEx( keyfobapp_TaskID, KFD_START_DEVICE_EVT, STARTDELAY );
#if defined(DEBUG_SERIAL)
    //添加串口初始化，并传递任务id
    //SerialApp_Init(task_id);
#endif
    SerialPrintString("Ready to Starting\r\n");
}

/*********************************************************************
 * @fn      KeyFobApp_ProcessEvent
 *
 * @brief   Key Fob Application Task event processor.  This function
 *          is called to process all events for the task.  Events
 *          include timers, messages and any other user defined events.
 *
 * @param   task_id  - The OSAL assigned task ID.
 * @param   events - events to process.  This is a bit map and can
 *                   contain more than one event.
 *
 * @return  none
 */
int gSYNCTimes = 0;
uint16 KeyFobApp_ProcessEvent( uint8 task_id, uint16 events )
{
    if ( events & SYS_EVENT_MSG )
    {
        uint8 *pMsg;

        if ( (pMsg = osal_msg_receive( keyfobapp_TaskID )) != NULL )
        {
            keyfobapp_ProcessOSALMsg( (osal_event_hdr_t *)pMsg );

            // Release the OSAL message
            VOID osal_msg_deallocate( pMsg );
        }

        // return unprocessed events
        return (events ^ SYS_EVENT_MSG);
    }

    if ( events & KFD_START_DEVICE_EVT )
    {
        // Start the Device
        GAPRole_StartDevice( (gapRolesCBs_t *)&keyFob_PeripheralCBs );
#if defined (ELARA_BOND)
        // Start Bond Manager
        VOID GAPBondMgr_Register( &keyFob_BondMgrCBs );
#endif
        // Set timer for first battery read event
        //osal_start_timerEx( keyfobapp_TaskID, KFD_BATTERY_CHECK_EVT, BATTERY_CHECK_PERIOD );
        //gBatteryValue = battMeasure();
        //advertDataSmall[12] = gBatteryValue;
        //advertDataBeacon[30] = gBatteryValue;
        //GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
        //osal_start_timerEx( keyfobapp_TaskID, KFD_POWERON_ADVERT_EVT, 600 );
        osal_start_timerEx( keyfobapp_TaskID, KFD_INIT_1_AVR_EVT, 3000 );
        HalLedSet( HAL_LED_2, HAL_LED_MODE_ON );
        HalLedSet( HAL_LED_2, HAL_LED_MODE_FLASH );
        SerialPrintString("BLE Stack is running\r\n");
        return ( events ^ KFD_START_DEVICE_EVT );
    }
    if ( events & KFD_POWERON_ADVERT_EVT)
    {
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            return ( events ^ KFD_POWERON_ADVERT_EVT );
        }
        else
        {
            uint8 current_adv_enabled_status;
            uint8 new_adv_enabled_status;
            //uint8 SK_Keys = SK_KEY_RIGHT;

            //Find the current GAP advertisement status
            GAPRole_GetParameter( GAPROLE_ADVERT_ENABLED, &current_adv_enabled_status );

            if( current_adv_enabled_status == FALSE )
            {
                new_adv_enabled_status = TRUE;
                //change the GAP advertisement status to opposite of current status
                GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &new_adv_enabled_status );
                //	SK_SetParameter( SK_KEY_ATTR, sizeof ( uint8 ), &SK_Keys );
            }
            //HalLedSet( HAL_LED_2, HAL_LED_MODE_ON );
            //HalLedSet( HAL_LED_2, HAL_LED_MODE_FLASH );
        }

#if defined ( POWER_SAVING )
        osal_pwrmgr_device( PWRMGR_BATTERY );
#endif
        //HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_OFF );
        //OnBoard_EnableKeys(true);

        //osal_start_timerEx( keyfobapp_TaskID, KFD_RESET, 86400000 );
        //osal_start_timerEx( keyfobapp_TaskID, KFD_INIT_AVR_EVT, 5000 );
        //HalSPIEraseBlockInit();
        //osal_start_timerEx( keyfobapp_TaskID, KFD_INIT_1_AVR_EVT, 3000 );
#if defined(STK500_ASYNCHRONOUS)
        gFSM = FSM_IDLE;
#endif
        return ( events ^ KFD_POWERON_ADVERT_EVT );
    }
    if ( events & KFD_RESET )
    {
        if (!(gapProfileState == GAPROLE_CONNECTED))
        {
            uint8 timerReset = 1;
            osal_snv_write(BLE_NVID_TIMER_RESET, 1, &timerReset);
            HAL_SYSTEM_RESET();
        }
        else
        {
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET, 3600000 );
        }
        return (events ^ KFD_RESET);
    }

    if ( events & KFD_RESET_NOW )
    {
        HAL_SYSTEM_RESET();
        //return (events ^ KFD_RESET_NOW);
    }

    if ( events & KFD_CONN_PARAM_UPDATE_EVT )
    {
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            //ELARA_LED2 = 1;
            //ELARA_LED1 = 0;
            // Send param update.  If it fails, retry until successful.
            GAPRole_SendUpdateParam( DEFAULT_DESIRED_MIN_CONN_INTERVAL, DEFAULT_DESIRED_MAX_CONN_INTERVAL,
                                     DEFAULT_DESIRED_SLAVE_LATENCY, DEFAULT_DESIRED_CONN_TIMEOUT,
                                     GAPROLE_NO_ACTION);
        }
        return (events ^ KFD_CONN_PARAM_UPDATE_EVT);
    }
    if ( events & KFD_BATTERY_CHECK_EVT )
    {
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            // Restart timer
            if ( BATTERY_CHECK_PERIOD )
            {
                osal_start_timerEx( keyfobapp_TaskID, KFD_BATTERY_CHECK_EVT, BATTERY_CHECK_PERIOD );
            }
        }

        // perform battery level check
        Batt_MeasLevel( );

        return (events ^ KFD_BATTERY_CHECK_EVT);
    }
    if (events & KFD_UART_INIT_EVT)
    {
        #if 1
        uint32 baudrate = HAL_UART_BR_57600;
        elara_snv_bond_read(&baudrate);
        KFD_HAL_DELAY(25000);// 200ms
        if (gElaraBaud == baudrate)
        {
            SerialApp_Init_2(keyfobapp_TaskID, 1, baudrate);
        }
        else
        {
            //KFD_HAL_DELAY(125000);// 1000ms
            //SerialApp_Init(keyfobapp_TaskID, 1, baudrate);
            SerialApp_Init_2(keyfobapp_TaskID, 1, baudrate);
        }
        KFD_HAL_DELAY(30000);// 240ms
        #endif
        osal_start_timerEx( keyfobapp_TaskID, KFD_POWERON_ADVERT_EVT, 600 );
        osal_start_timerEx( keyfobapp_TaskID, KFD_LED_BLINK_EVT, 102 );
        return (events ^ KFD_UART_INIT_EVT);
    }
    #ifdef ELARA_UNINIT_UART
    if (events & KFD_UART_UNINIT_EVT)
    {
        SerialApp_Uninit();
        osal_start_timerEx( keyfobapp_TaskID, KFD_POWERON_ADVERT_EVT, 600 );
        osal_start_timerEx( keyfobapp_TaskID, KFD_LED_BLINK_EVT, 102 );
        return (events ^ KFD_UART_UNINIT_EVT);
    }
    #endif
    if ( events & KFD_UART_EVT )
    {
#if 0
        if (gapProfileState != GAPROLE_CONNECTED)
        {
            return (events ^ KFD_UART_EVT);
        }
#else
        if (gManualTerminate != 0)
        {
            return (events ^ KFD_UART_EVT);
        }
#endif
        uint8  pktBuffer[SBP_UART_RX_BUF_SIZE] = {0};

        uint16 sizes = Hal_UART_RxBufLen(0);
        if (sizes == 0)
        {
            osal_start_timerEx( keyfobapp_TaskID, KFD_UART_EVT, 10 );
            return (events ^ KFD_UART_EVT);
        }
        uint16 len = HalUARTRead (0, pktBuffer, SBP_UART_RX_BUF_SIZE);
        if (gapProfileState != GAPROLE_CONNECTED)
        {
            bool parseRET = parse_at_command(pktBuffer, len);
            if (parseRET == TRUE)
            {
                osal_start_timerEx( keyfobapp_TaskID, KFD_UART_EVT, 1000 );
                return (events ^ KFD_UART_EVT);
            }
            else
            {
                osal_start_timerEx( keyfobapp_TaskID, KFD_UART_EVT, 1000 );
                return (events ^ KFD_UART_EVT);
            }
        }
        if (FALSE == MDUARTSerialAppSendNoti(pktBuffer, len))
        {
            if (len != 0)
            {
                uint8 tempBuffer[50] = {0};
                sprintf((char *)tempBuffer, "Please connect me first!!!\r\n");
                HalUARTWrite (0, tempBuffer, strlen((char const *)tempBuffer));
            }
        }
        osal_start_timerEx( keyfobapp_TaskID, KFD_UART_EVT, 10 );
        return (events ^ KFD_UART_EVT);
    }
#if 0
    if ( events & KFD_UART_EVT )
    {
        static bool haha = TRUE;
        if (haha)
            P1_4 = 1;//Yellow LED
        else
            P1_4 = 0;
        haha = !haha;
        int rv;
        gSYNCTimes = 5;
        SerialPrintString("OH, Who are you? I am Li Xianyu.\r\n");
        //uint8 v = sizeof(short);
        SerialPrintValue("sizeof(short) = ", v, 10);
        //int rv = elara_fsm();
        //rv = rv;
        advertDataBeacon[27] = (rv >> 8) & 0xFF;
        advertDataBeacon[28] = rv & 0xFF;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
        return (events ^ KFD_UART_EVT);
    }
#else
    if ( events & KFD_TERMINATE_EVT )
    {
        if (gManualTerminate == 1)
        {
            gManualTerminate = 2;
            GAPRole_TerminateConnection();
            return (events ^ KFD_TERMINATE_EVT);
        }
        if (gWriteDone == MDPROFILE_ERASE)
        {
            uint32 uState = 3;
#if 1
            elara_snv_write(BLE_NVID_U_STATE, 1, &uState);
#endif
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_NOW, 5200 );
        }
        else if (gWriteDone == MDPROFILE_UPDATE_AVR)
        {
            uint32 partID = m1284p_16M_5V;
            MDProfile_GetParameter(MDPROFILE_PARTID, &partID);
#if 1
            elara_snv_write(BLE_NVID_PART_ID, 1, &partID);
#endif
            elara_flash_write_done();
            gUAS = UPDATE_AVR_CONN_PARAM_UPDATE;
            osal_start_timerEx( keyfobapp_TaskID, KFD_UPDATE_AVR_EVT, 1000 );
            char strTemp[32] = "Begin update";
            MDSerialAppSendNoti((uint8*)strTemp, 12);
        }
        else if (gWriteDone == MDPROFILE_RESET_MANUAL)
        {
            MDBeforeReset();
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_NOW, 4200 );
        }
        return (events ^ KFD_TERMINATE_EVT);
    }
#endif
    if (events & KFD_UPDATE_AVR_EVT)
    {
        UpdateAVRevent();
        return (events ^ KFD_UPDATE_AVR_EVT);
    }
    if (events & KFD_INIT_1_AVR_EVT)
    {
        processAVRevent();
        return (events ^ KFD_INIT_1_AVR_EVT);
    }
#if 1
    if (events & KFD_ERASE_EVT)
    {
        ELARA_LED1 = !ELARA_LED1;
        ELARA_LED2 = !ELARA_LED2;
        //if (elara_flash_if_erase_finished() == TRUE)
        if (TRUE == HalSPIEraseChipFinished())
        {
            ELARA_LED1 = 1;
            ELARA_LED2 = 1;
            uint32 uState = 0;
#if 1
            elara_snv_write(BLE_NVID_U_STATE, 1, &uState);
#endif
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_NOW, 8000 );
        }
        else
        {
            // Next check
            osal_start_timerEx( keyfobapp_TaskID, KFD_ERASE_EVT, 200 );
        }
        return (events ^ KFD_ERASE_EVT);
    }
#else
    if (events & KFD_ERASE_EVT)
    {
        ELARA_LED1 = !ELARA_LED1;
        ELARA_LED2 = !ELARA_LED2;
        if (0 == HalSPIErase64KBBlock())
        {
            ELARA_LED1 = 1;
            ELARA_LED2 = 1;
            uint32 uState = 0;
            elara_snv_write(BLE_NVID_U_STATE, 1, &uState);
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_NOW, 8000 );
        }
        else
        {
            // Next check
            osal_start_timerEx( keyfobapp_TaskID, KFD_ERASE_EVT, 200 );
        }
        return (events ^ KFD_ERASE_EVT);
    }
#endif
#if 0
    if (events & KFD_JUST_FOR_TEST_FLASH_EVT)
    {
        int rc = 0;
        //rc = justForTestFlash();
        advertDataBeacon[29] = (rc >> 8) & 0xFF;
        advertDataBeacon[30] = rc & 0xFF;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
        osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_NOW, 5000 );
        //elara_flash_test_read_init();
        //osal_start_timerEx( keyfobapp_TaskID, KFD_JUST_FOR_TEST1_FLASH_EVT, 2000 );
        return (events ^ KFD_JUST_FOR_TEST_FLASH_EVT);
    }
    if (events & KFD_JUST_FOR_TEST1_FLASH_EVT)
    {
        static uint16 counts = 0;
        uint8 rcc = elara_flash_test_read();
        //int rc = elara_flash_write_done();
        //advertDataBeacon[29] = (rc >> 8) & 0xFF;
        //advertDataBeacon[30] = rc & 0xFF;

        if (counts++ < 292)
            osal_start_timerEx( keyfobapp_TaskID, KFD_JUST_FOR_TEST1_FLASH_EVT, 2000 );
        advertDataBeacon[29] = counts;
        advertDataBeacon[30] = rcc;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
        return (events ^ KFD_JUST_FOR_TEST1_FLASH_EVT);
    }
#else
    
#endif
#if defined(STK500_ASYNCHRONOUS)
    if (events & KFD_INIT_AVR_EVT)
    {
#if defined(DEBUG_SERIAL)
        osal_start_timerEx( keyfobapp_TaskID, KFD_TEST_EVT, 6000 );
        uint16 usedmem = 0;
        usedmem = osal_heap_mem_used();
        SerialPrintValue("BLE used memory is : ", usedmem, 10);
#endif
        SerialPrintString("Begin m328p_init()\r\n");
        int rc = m328p_init();
        rc = -rc;
        advertDataBeacon[29] = (rc >> 8) & 0xFF;
        advertDataBeacon[30] = rc & 0xFF;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
        SerialPrintString("Finish m328p_init()\r\n");
#if defined(DEBUG_SERIAL)
        usedmem = osal_heap_mem_used();
        SerialPrintValue("Now BLE used memory is : ", usedmem, 10);
#endif
        m328p_open();
        gFSM = FSM_getsync;
#if 0
        rc = m328p_getsync();
        gSYNCTimes++;
        advertDataBeacon[25] = (rc >> 8) & 0xFF;
        advertDataBeacon[26] = rc & 0xFF;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
#endif
        osal_start_timerEx( keyfobapp_TaskID, KFD_SYNC_EVT, 5 );
        return (events ^ KFD_INIT_AVR_EVT);
    }

    if (events & KFD_SYNC_EVT)
    {
        ELARA_LED1 = 1;
        if (gFSM == FSM_getsync)
        {
            if (gSYNCTimes < 3)
            {
                m328p_getsync();
                gSYNCTimes++;
                osal_start_timerEx( keyfobapp_TaskID, KFD_SYNC_EVT, 50 );
            }
            else
            {
                gSYNCTimes = 0;
                int rv = elara_fsm();
                if (rv > 0)
                {
                    ELARA_LED1 = 0;
                    gFSM = FSM_GET_MAJOR;
                    m328p_getparm(Parm_STK_SW_MAJOR);
                    osal_start_timerEx( keyfobapp_TaskID, KFD_GET_PARM_EVT, 50 );
                }
                advertDataBeacon[27] = (rv >> 8) & 0xFF;
                advertDataBeacon[28] = rv & 0xFF;
                GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
            }
        }
        //KFD_HAL_DELAY(12500);
        return (events ^ KFD_SYNC_EVT);
    }
    if (events & KFD_GET_PARM_EVT)
    {
        int rv = elara_fsm();
        if (rv == 0)
        {
            //m328p_getparm(Parm_STK_SW_MAJOR);
            if (gSYNCTimes++ < 4)
            {
                osal_start_timerEx( keyfobapp_TaskID, KFD_GET_PARM_EVT, 500 );
            }
            else
            {
                gSYNCTimes = 0;
                gFSM = FSM_getsync;
                osal_start_timerEx( keyfobapp_TaskID, KFD_SYNC_EVT, 5 );
            }
        }
        else
        {
            P0_1 = 1;// Orange LED.
        }
        return (events ^ KFD_GET_PARM_EVT);
    }
    if (events & KFD_INITIALIZE_EVT)
    {

        return (events ^ KFD_INITIALIZE_EVT);
    }
#endif
    if (events & KFD_TEST_EVT)
    {
#if defined(DEBUG_SERIAL)
        uint16 usedmem = 0;
        usedmem = osal_heap_mem_used();
        SerialPrintValue("Now BLE used memory is : ", usedmem, 10);
        osal_start_timerEx( keyfobapp_TaskID, KFD_TEST_EVT, 3000 );
#endif
        uint32 baudrate = HAL_UART_BR_57600;
        elara_snv_bond_read(&baudrate);
        SerialApp_Init(keyfobapp_TaskID, 1, baudrate);
        #if 0
        uint8 pktBuffer[SBP_UART_RX_BUF_SIZE] = {0};
        HalUARTRead (0, pktBuffer, SBP_UART_RX_BUF_SIZE);
        #endif
        //osal_start_timerEx( keyfobapp_TaskID, KFD_UART_EVT, 800 );
        return (events ^ KFD_TEST_EVT);
    }

#if defined ( PLUS_BROADCASTER )
    if ( events & KFD_ADV_IN_CONNECTION_EVT )
    {
        uint8 turnOnAdv = TRUE;
        // Turn on advertising while in a connection
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &turnOnAdv );
        return (events ^ KFD_ADV_IN_CONNECTION_EVT);
    }
#endif

    if (events & KFD_LED_BLINK_EVT)
    {
        static bool ledFirst = TRUE;
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            ELARA_LED1 = 0;
            ELARA_LED2 = 0;
            return (events ^ KFD_LED_BLINK_EVT);
        }
        if (ledFirst == TRUE)
        {
            if (ELARA_LED1 != ELARA_LED2)
            {
                ELARA_LED2 = ELARA_LED1;
            }
            ledFirst = FALSE;
        }
        ELARA_LED1 = !ELARA_LED1;
        ELARA_LED2 = !ELARA_LED2;
        osal_start_timerEx( keyfobapp_TaskID, KFD_LED_BLINK_EVT, 891 );
        return (events ^ KFD_LED_BLINK_EVT);
    }
    // Discard unknown events
    return 0;
}

#if 0
uint16 KeyFobApp_ProcessEvent1( uint8 task_id, uint16 events, uint8 *pbuf)
{
    if ( events & SYS_EVENT_MSG )
    {
        uint8 *pMsg;

        if ( (pMsg = osal_msg_receive( keyfobapp_TaskID )) != NULL )
        {
            keyfobapp_ProcessOSALMsg( (osal_event_hdr_t *)pMsg );

            // Release the OSAL message
            VOID osal_msg_deallocate( pMsg );
        }

        // return unprocessed events
        return (events ^ SYS_EVENT_MSG);
    }

    if ( events & KFD_START_DEVICE_EVT )
    {
        // Start the Device
        VOID GAPRole_StartDevice( &keyFob_PeripheralCBs );

        // Start Bond Manager
        VOID GAPBondMgr_Register( &keyFob_BondMgrCBs );

        // Set timer for first battery read event
        //osal_start_timerEx( keyfobapp_TaskID, KFD_BATTERY_CHECK_EVT, BATTERY_CHECK_PERIOD );
        gBatteryValue = pbuf[32768];//battMeasure();
        advertDataSmall[12] = gBatteryValue;
        advertDataBeacon[30] = gBatteryValue;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
        osal_start_timerEx( keyfobapp_TaskID, KFD_POWERON_ADVERT_EVT, 600 );
        return ( events ^ KFD_START_DEVICE_EVT );
    }
    if ( events & KFD_POWERON_ADVERT_EVT)
    {
        {
            uint8 current_adv_enabled_status;
            uint8 new_adv_enabled_status;
            //uint8 SK_Keys = SK_KEY_RIGHT;

            //Find the current GAP advertisement status
            GAPRole_GetParameter( GAPROLE_ADVERT_ENABLED, &current_adv_enabled_status );

            if( current_adv_enabled_status == FALSE )
            {
                new_adv_enabled_status = TRUE;
                //change the GAP advertisement status to opposite of current status
                GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &new_adv_enabled_status );
                //	SK_SetParameter( SK_KEY_ATTR, sizeof ( uint8 ), &SK_Keys );
            }
            HalLedSet( HAL_LED_2, HAL_LED_MODE_ON );
            HalLedSet( HAL_LED_2, HAL_LED_MODE_FLASH );
        }

#if defined ( POWER_SAVING )
        osal_pwrmgr_device( PWRMGR_BATTERY );
#endif
        //HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_OFF );
        //OnBoard_EnableKeys(true);

        osal_start_timerEx( keyfobapp_TaskID, KFD_RESET, 86400000 );
        //avr_init();
        return ( events ^ KFD_POWERON_ADVERT_EVT );
    }
    if ( events & KFD_RESET )
    {
        if (!(gapProfileState == GAPROLE_CONNECTED))
        {
            uint8 timerReset = 1;
            osal_snv_write(BLE_NVID_TIMER_RESET, 1, &timerReset);
            HAL_SYSTEM_RESET();
        }
        else
        {
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET, 3600000 );
        }
        return (events ^ KFD_RESET);
    }

    if ( events & KFD_RESET_NOW )
    {
        HAL_SYSTEM_RESET();
        //return (events ^ KFD_RESET_NOW);
    }

    if ( events & KFD_CONN_PARAM_UPDATE_EVT )
    {
        //SerialPrintValue("Enter peripheralStateNotificationCB:newState=", newState, 10);
        SerialPrintString("KFD_CONN_PARAM_UPDATE_EVT enter\r\n");
        // Send param update.  If it fails, retry until successful.
        GAPRole_SendUpdateParam( DEFAULT_DESIRED_MIN_CONN_INTERVAL, DEFAULT_DESIRED_MAX_CONN_INTERVAL,
                                 DEFAULT_DESIRED_SLAVE_LATENCY, DEFAULT_DESIRED_CONN_TIMEOUT,
                                 GAPROLE_NO_ACTION);
        return (events ^ KFD_CONN_PARAM_UPDATE_EVT);
    }
    if ( events & KFD_BATTERY_CHECK_EVT )
    {
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            // Restart timer
            if ( BATTERY_CHECK_PERIOD )
            {
                osal_start_timerEx( keyfobapp_TaskID, KFD_BATTERY_CHECK_EVT, BATTERY_CHECK_PERIOD );
            }
        }

        // perform battery level check
        Batt_MeasLevel( );

        return (events ^ KFD_BATTERY_CHECK_EVT);
    }

#if defined ( PLUS_BROADCASTER )
    if ( events & KFD_ADV_IN_CONNECTION_EVT )
    {
        uint8 turnOnAdv = TRUE;
        // Turn on advertising while in a connection
        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &turnOnAdv );
        return (events ^ KFD_ADV_IN_CONNECTION_EVT);
    }
#endif

    // Discard unknown events
    return 0;
}
#endif

/*********************************************************************
 * @fn      keyfobapp_ProcessOSALMsg
 *
 * @brief   Process an incoming task message.
 *
 * @param   pMsg - message to process
 *
 * @return  none
 */
static void keyfobapp_ProcessOSALMsg( osal_event_hdr_t *pMsg )
{
    switch ( pMsg->event )
    {
    case KEY_CHANGE:
        keyfobapp_HandleKeys( ((keyChange_t *)pMsg)->state, ((keyChange_t *)pMsg)->keys );
        break;
    }
}

/*********************************************************************
 * @fn      keyfobapp_HandleKeys
 *
 * @brief   Handles all key events for this device.
 *
 * @param   shift - true if in shift/alt.
 * @param   keys - bit field for key events. Valid entries:
 *                 HAL_KEY_SW_2
 *                 HAL_KEY_SW_1
 *
 * @return  none
 */
static void keyfobapp_HandleKeys( uint8 shift, uint8 keys )
{
    uint8 SK_Keys = 0;

    (void)shift;  // Intentionally unreferenced parameter

    if ( keys & HAL_KEY_SW_1 )
    {
    }

    if ( keys & HAL_KEY_SW_2 )
    {
    }

    SK_SetParameter( SK_KEY_ATTR, sizeof ( uint8 ), &SK_Keys );
}


/*********************************************************************
 * @fn      phobosRateBattCB
 *
 * @brief   Callback function for battery service.
 *
 * @param   event - service event
 *
 * @return  none
 */
static void phobosRateBattCB(uint8 event)
{
    if (event == BATT_LEVEL_NOTI_ENABLED)
    {
        // if connected start periodic measurement
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            osal_start_timerEx( keyfobapp_TaskID, KFD_BATTERY_CHECK_EVT, BATTERY_CHECK_PERIOD );
        }
        //HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_BLINK );
    }
    else if (event == BATT_LEVEL_NOTI_DISABLED)
    {
        // stop periodic measurement
        osal_stop_timerEx( keyfobapp_TaskID, KFD_BATTERY_CHECK_EVT );
    }
}

/*********************************************************************
 * @fn      peripheralStateNotificationCB
 *
 * @brief   Notification from the profile of a state change.
 *
 * @param   newState - new state
 *
 * @return  none
 */
static void peripheralStateNotificationCB( gaprole_States_t newState )
{
    //  uint16 connHandle = INVALID_CONNHANDLE;
    //uint8 valFalse = FALSE;
    //SerialPrintString("Enter peripheralStateNotificationCB()\r\n");
    SerialPrintValue("Enter peripheralStateNotificationCB:newState=", newState, 10);
    SerialPrintString("\r\n");
    if ( gapProfileState != newState )
    {
#if 0
        if (gapProfileState == GAPROLE_CONNECTED)
        {
            if (newState == GAPROLE_INIT || newState == GAPROLE_STARTED ||
                    newState == GAPROLE_ADVERTISING || newState == GAPROLE_ADVERTISING_NONCONN ||
                    newState == GAPROLE_WAITING || newState == GAPROLE_WAITING_AFTER_TIMEOUT ||
                    newState == GAPROLE_CONNECTED_ADV)
            {
                P0_1 = 1; // Orange LED.
                ELARA_LED1 = 1;
            }
            else if (newState == GAPROLE_ERROR)
            {
                P0_1 = 1; // Orange LED.
            }
            else
            {
                ELARA_LED1 = 1;
            }
        }
#endif
        if (gapProfileState == GAPROLE_CONNECTED &&
                newState != GAPROLE_CONNECTED)
        {
            GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon_OLD ), (void *)advertDataBeacon_OLD );
            osal_start_timerEx( keyfobapp_TaskID, KFD_LED_BLINK_EVT, 891 );
            ELARA_LED1 = 0;
            ELARA_LED2 = 0;
            uint8 advState = TRUE;

            // link loss timeout-- use fast advertising
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, DEFAULT_DISC_ADV_INT_MIN );
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, DEFAULT_DISC_ADV_INT_MIN );
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 10000 );
            // Enable advertising
            GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
            //HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_ON );
            if (gManualTerminate == 0)
            {
                // Do nothing!
            }
            else if (gManualTerminate == 1)
            {
                if (newState != GAPROLE_ERROR)
                {
                    elara_flash_lost_connected();
                    osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_NOW, 4090 );
                }
            }
            else if (gManualTerminate == 2)
            {
                osal_start_timerEx( keyfobapp_TaskID, KFD_TERMINATE_EVT, 100 );
            }
        }
        // if advertising stopped
        else if ( gapProfileState == GAPROLE_ADVERTISING &&
                  newState == GAPROLE_WAITING )
        {
            GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), (void *)advertDataBeacon );
            uint8 advState = TRUE;
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MIN, DEFAULT_DISC_ADV_INT_MAX );
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_INT_MAX, DEFAULT_DISC_ADV_INT_MAX );
            GAP_SetParamValue( TGAP_GEN_DISC_ADV_MIN, 0 );
            GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &advState );
            HalLedSet( HAL_LED_2, HAL_LED_MODE_OFF );
        }

        switch( newState )
        {
        case GAPROLE_STARTED:
        {
            // Set the system ID from the bd addr
            uint8 systemId[DEVINFO_SYSTEM_ID_LEN];
            uint8 systemIdBak[DEVINFO_SYSTEM_ID_LEN];
            GAPRole_GetParameter(GAPROLE_BD_ADDR, systemId);
            //osal_memcpy(&advertDataBeacon[25], systemId, B_ADDR_LEN);

            //osal_memcpy(&advertDataSmall[5], systemId, B_ADDR_LEN);
            osal_memcpy(systemIdBak, systemId, DEVINFO_SYSTEM_ID_LEN);

            // shift three bytes up
            systemId[7] = systemId[5];
            systemId[6] = systemId[4];
            systemId[5] = systemId[3];

            // set middle bytes to zero
            systemId[4] = 0;
            systemId[3] = 0;
            DevInfo_SetParameter(DEVINFO_SYSTEM_ID, DEVINFO_SYSTEM_ID_LEN, systemId);

            // Set the serial number from the bd addr.
            uint8 serialNumber[DEVINFO_SERIAL_NUMBER_LEN + 2] = "\0";
            uint8 aNumber;
            uint8 j = 0;
            for (int8 i = B_ADDR_LEN - 1; i >= 0; i--)
            {
                aNumber = systemIdBak[i];
                if (aNumber < 16)
                {
                    strcat((char *)serialNumber + j * 2, (const char *)"0");
                    _itoa(aNumber, serialNumber + j * 2 + 1, 16);
                }
                else
                {
                    _itoa(aNumber, serialNumber + j * 2, 16);
                }

                /*if (osal_memcmp(&aNumber, &Zero, 1) == TRUE)
                {
                    strcat((char*)serialNumber+j*2+1, (const char*)"0");
                }*/
                j++;
            }
            DevInfo_SetParameter(DEVINFO_SERIAL_NUMBER, DEVINFO_SERIAL_NUMBER_LEN, serialNumber);
            osal_memcpy(elaraScanResponseData + 9, serialNumber+8, 4);
            GAPRole_SetParameter( GAPROLE_SCAN_RSP_DATA, sizeof ( elaraScanResponseData ), (void *)elaraScanResponseData );

            osal_memcpy(elara_attDeviceName + 7, serialNumber+8, 4);
            GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, (void *)elara_attDeviceName );
        }
        break;

        //if the state changed to connected, initially assume that keyfob is in range
        case GAPROLE_ADVERTISING:
        {
            // Visual feedback that we are advertising.
            //HalLedSet( (HAL_LED_1 | HAL_LED_2), HAL_LED_MODE_ON );
        }
        break;

        //if the state changed to connected, initially assume that keyfob is in range
        case GAPROLE_CONNECTED:
        {

            //GAPRole_GetParameter( GAPROLE_CONNHANDLE, &connHandle );

#if defined ( PLUS_BROADCASTER )
            osal_start_timerEx( keyfobapp_TaskID, KFD_ADV_IN_CONNECTION_EVT, ADV_IN_CONN_WAIT );
#endif

            // Turn off LED that shows we're advertising
            HalLedSet(HAL_LED_2 | HAL_LED_1, HAL_LED_MODE_OFF );

            // Set timer to update connection parameters
            // 5 seconds should allow enough time for Service Discovery by the collector to finish
            //osal_start_timerEx( keyfobapp_TaskID, KFD_CONN_PARAM_UPDATE_EVT, 5000);
        }
        break;

        case GAPROLE_WAITING:
        {

        }
        break;

        case GAPROLE_WAITING_AFTER_TIMEOUT:
        {

        }
        break;

        default:
            // do nothing
            break;
        }
    }

    gapProfileState = newState;
}

/*********************************************************************
 * @fn      ccServiceChangeCB
 *
 * @brief   Callback from Connection Control indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
static void ccServiceChangeCB( uint8 paramID )
{

    // CCSERVICE_CHAR1: read & notify only

    // CCSERVICE_CHAR: requested connection parameters
    if( paramID == CCSERVICE_CHAR2 )
    {
        uint8 buf[CCSERVICE_CHAR2_LEN];
        uint16 minConnInterval;
        uint16 maxConnInterval;
        uint16 slaveLatency;
        uint16 timeoutMultiplier;

        CcService_GetParameter( CCSERVICE_CHAR2, buf );

        minConnInterval = BUILD_UINT16(buf[0], buf[1]);
        maxConnInterval = BUILD_UINT16(buf[2], buf[3]);
        slaveLatency = BUILD_UINT16(buf[4], buf[5]);
        timeoutMultiplier = BUILD_UINT16(buf[6], buf[7]);

        //osal_start_timerEx( keyfobapp_TaskID, KFD_CONN_PARAM_UPDATE_EVT, 5000);
        // Update connection parameters
        GAPRole_SendUpdateParam( minConnInterval, maxConnInterval,
                                 slaveLatency, timeoutMultiplier, GAPROLE_NO_ACTION);
    }

    // CCSERVICE_CHAR3: Disconnect request
    if( paramID == CCSERVICE_CHAR3 )
    {
        // Any change in the value will terminate the connection
        GAPRole_TerminateConnection();
    }
}

#if defined(GLIESE)
static void glieseServiceChangeCB( uint8 paramID )
{
    if ( paramID == GLIESEPROFILE_CHAR3 )
    {
        uint8 buf[GAP_DEVICE_NAME_LEN] = {0};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR3, buf);
        //GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN, elara_attDeviceName );
        GGS_SetParameter( GGS_DEVICE_NAME_ATT, GAP_DEVICE_NAME_LEN - 3, buf );
        osal_snv_write(BLE_NVID_PHOBOS_DEVICE_NAME, GAP_DEVICE_NAME_LEN, buf);
        return;
    }
    if ( paramID == GLIESEPROFILE_CHAR1 )
    {
        uint8 beacon[GLIESEPROFILE_CHAR1_LEN] = {0};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR1, beacon);
        uint8 password[4] = {0xFF, 0x02, 0x9D, 0x8E};
        if (FALSE == osal_memcmp(password, beacon + 7, 4))
        {
            return;
        }
        // set major
        advertDataBeacon[25] = beacon[0];
        advertDataBeacon[26] = beacon[1];

        // set minor
        advertDataBeacon[27] = beacon[2];
        advertDataBeacon[28] = beacon[3];
        GlieseProfile_SetParameter(GLIESEPROFILE_CHAR1, GLIESEPROFILE_CHAR1_LEN - 4, beacon);
        // set The 2's complement of the calibrated Tx Power
        advertDataBeacon[29] = beacon[4];

        //Tx Power
        uint8 txPower = HCI_EXT_TX_POWER_0_DBM;
        switch (beacon[5])
        {
        case 0:
            txPower = HCI_EXT_TX_POWER_MINUS_23_DBM;
            break;

        case 1:
            txPower = HCI_EXT_TX_POWER_MINUS_6_DBM;
            break;

        case 2:
            txPower = HCI_EXT_TX_POWER_0_DBM;
            break;

        case 3:
            txPower = HCI_EXT_TX_POWER_0_DBM;
            break;
        }
        HCI_EXT_SetTxPowerCmd( txPower );

        //Advertising Freq (1-100) Unit: 100ms
        if (beacon[6] != 0)
        {
            gAdvIntervalMin = (uint16)((float)beacon[6] * (float)100 / (float)0.625);
            if (gAdvIntervalMin < 1600)
            {
                gAdvIntervalMin = 1600;
            }
            gAdvIntervalMax = gAdvIntervalMin;
        }
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );

        osal_snv_write(BLE_NVID_iBeacon_SET1, GLIESEPROFILE_CHAR1_LEN - 4, beacon);
        return;
    }

    if ( paramID == GLIESEPROFILE_CHAR10 )
    {
        uint8 buffer[GLIESEPROFILE_CHAR10_LEN] = {0};
        uint8 password[6] = {0x61, 0x88, 0xC6, 0xA1, 0xA4, 0x16};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR10, buffer);
        if (TRUE == osal_memcmp(buffer, password, 6))
        {
            uint32 passkey = ((uint32)buffer[6] << 24) |
                             ((uint32)buffer[7] << 16) |
                             ((uint32)buffer[8] << 8) |
                             ((uint32)buffer[9]);//DEFAULT_PHOBOS_PASSCODE;
            uint8 pairMode = buffer[10];//GAPBOND_PAIRING_MODE_WAIT_FOR_REQ;
            uint8 mitm = buffer[11];//TRUE;
            uint8 ioCap = buffer[12];//GAPBOND_IO_CAP_DISPLAY_ONLY;
            uint8 bonding = buffer[13];//TRUE;

            gDefaultPasscode = passkey;
            GAPBondMgr_SetParameter( GAPBOND_DEFAULT_PASSCODE, sizeof ( uint32 ), &passkey );
            GAPBondMgr_SetParameter( GAPBOND_PAIRING_MODE, sizeof ( uint8 ), &pairMode );
            GAPBondMgr_SetParameter( GAPBOND_MITM_PROTECTION, sizeof ( uint8 ), &mitm );
            GAPBondMgr_SetParameter( GAPBOND_IO_CAPABILITIES, sizeof ( uint8 ), &ioCap );
            GAPBondMgr_SetParameter( GAPBOND_BONDING_ENABLED, sizeof ( uint8 ), &bonding );

            osal_snv_write(BLE_NVID_BOND_SETTING, 8, &buffer[6]);
        }
        return;
    }

    if (paramID == GLIESEPROFILE_CHAR2)
    {
        uint8 beacon[GLIESEPROFILE_CHAR2_LEN] = {0};
        GlieseProfile_GetParameter(GLIESEPROFILE_CHAR2, beacon);
        osal_memcpy(advertDataBeacon + 9, beacon, 16);
        osal_snv_write(BLE_NVID_iBeacon_SET, GLIESEPROFILE_CHAR2_LEN, beacon);
        return;
    }
}
#endif

#if defined (ELARA_BOND)
//绑定过程中的密码管理回调函数
static void ProcessPasscodeCB(uint8 *deviceAddr, uint16 connectionHandle,
                              uint8 uiInputs, uint8 uiOutputs )
{
    //uint32  passcode;
    //uint8   str[7];

    //在这里可以设置存储，保存之前设定的密码，这样就可以动态修改配对密码了。
    // Create random passcode
    /*LL_Rand( ((uint8 *) &passcode), sizeof( uint32 ));
    passcode %= 1000000;
    */

    //在lcd上显示当前的密码，这样手机端，根据此密码连接。
    // Display passcode to user
    /*if ( uiOutputs != 0 )
    {
      HalLcdWriteString( "Passcode:",  HAL_LCD_LINE_1 );
      HalLcdWriteString( (char *) _ltoa(passcode, str, 10),  HAL_LCD_LINE_2 );
    }*/

    // Send passcode response
    GAPBondMgr_PasscodeRsp( connectionHandle, SUCCESS, gDefaultPasscode);
}

//绑定过程中的状态管理，在这里可以设置标志位，当密码不正确时不允许连接。
static void ProcessPairStateCB( uint16 connHandle, uint8 state, uint8 status )
{
    if ( state == GAPBOND_PAIRING_STATE_STARTED )/*主机发起连接，会进入开始绑定状态*/
    {
        //HalLcdWriteString( "Pairing started", HAL_LCD_LINE_1 );
        gPairStatus = 0;
    }
    else if ( state == GAPBOND_PAIRING_STATE_COMPLETE )/*当主机提交密码后，会进入完成*/
    {
        if ( status == SUCCESS )
        {
            //HalLcdWriteString( "Pairing success", HAL_LCD_LINE_1 );/*密码正确*/
            gPairStatus = 1;
        }
        else
        {
            //HalLcdWriteStringValue( "Pairing fail", status, 10, HAL_LCD_LINE_1 );/*密码不正确，或者先前已经绑定*/
            if(status == 8) /*已绑定*/
            {
                gPairStatus = 1;
            }
            else
            {
                gPairStatus = 0;
            }
        }
        //判断配对结果，如果不正确立刻停止连接。
        if(gapProfileState == GAPROLE_CONNECTED && gPairStatus != 1)
        {
            GAPRole_TerminateConnection();
        }
    }
    else if ( state == GAPBOND_PAIRING_STATE_BONDED )
    {
        if ( status == SUCCESS )
        {
            //HalLcdWriteString( "Bonding success", HAL_LCD_LINE_1 );
        }
    }
}
#endif

// 判断蓝牙是否连接上
// 0: 未连接上
// 1: 已连接上
bool Elara_IfConnected(void)
{
    return (gapProfileState == GAPROLE_CONNECTED);    
}

/*********************************************************************
 * @fn      simpleProfileChangeCB
 *
 * @brief   Callback from SimpleBLEProfile indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
static void simpleProfileChangeCB( uint8 paramID )
{
    uint8 newValue;

    switch( paramID )
    {
    case SIMPLEPROFILE_CHAR1:
        SimpleProfile_GetParameter( SIMPLEPROFILE_CHAR1, &newValue );
        break;

    case SIMPLEPROFILE_CHAR3:
        SimpleProfile_GetParameter( SIMPLEPROFILE_CHAR3, &newValue );
        break;

    default:
        // should not reach here!
        break;
    }
}

static void md_ProfileChangeCB( uint8 paramID )
{
    //uint8 newValueBuf[MD_TRANS_LEN] = {0};
    switch( paramID )
    {
    case MDPROFILE_ERASE:
        //osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_NOW, 5000 );
        gWriteDone = MDPROFILE_ERASE;
        osal_start_timerEx( keyfobapp_TaskID, KFD_TERMINATE_EVT, 3000 );
        break;
    case MDPROFILE_UPDATE_AVR:
        gWriteDone = MDPROFILE_UPDATE_AVR;
        osal_start_timerEx( keyfobapp_TaskID, KFD_TERMINATE_EVT, 2300 );
        break;
    case MDPROFILE_RESET_MANUAL:
        gWriteDone = MDPROFILE_RESET_MANUAL;
        osal_start_timerEx( keyfobapp_TaskID, KFD_TERMINATE_EVT, 3000 );
        break;
    case MDPROFILE_PARTID:
        //        MDProfile_GetParameter
        break;
    case MDPROFILE_TRANS:
        resolve_command();
        break;
    default:
        // should not reach here!
        break;
    }
    if (paramID != MDPROFILE_TRANS)
    {
        osal_start_timerEx( keyfobapp_TaskID, KFD_LED_BLINK_EVT, 100 );
    }
}
static void md_UARTProfileChangeCB( uint8 paramID )
{
    switch( paramID )
    {
     case MD_UART_PROFILE_TRANS:
        resolve_command();
        break;
    default:
        // should not reach here!
        break;
    }
}

#define HEADER_MAGIC  (0xAB)
static void resolve_command(void)
{
    uint8 returnBytes;
    uint8 command_id;
    uint8 data[MD_UART_TRANS_LEN] = {0};
    MDUARTProfile_GetParameter( MD_UART_PROFILE_TRANS, data, &returnBytes);
    if(data[0] != HEADER_MAGIC)
    {
        //sbpSerialAppWrite(data, strlen((char const*)data));
        HalUARTWrite (SBP_UART_PORT, data, returnBytes);
        return;
    }
    command_id = data[1];
    uint32 baudrate = HAL_UART_BR_115200;
    
    switch (command_id)
    {
    case 0x00:
    case 0x01:
    case 0x02:
    case 0x03:
    case 0x04:
        baudrate = command_id;
        elara_snv_bond_write(&baudrate);
        KFD_HAL_DELAY(25000);// 200ms
        HAL_SYSTEM_RESET();  //直接重启即可
        break;
    default:
        sbpSerialAppWrite(data, strlen((char const*)data));
        break;
    }
}

#if defined(STK500_ASYNCHRONOUS)
static int g_attempt = 0;
static int elara_fsm(void)
{
    unsigned char buf[128] = {0};
    int rv = -1;

    switch(gFSM)
    {
    case FSM_IDLE:
        //P0_1 = 1; // Orange LED.
        rv = m328p_recv(buf, sizeof(buf)); // Just drain.
        break;
    case FSM_getsync:
        //P1_4 = 0;//Yellow LED.

        rv = m328p_recv(buf, sizeof(buf));
        if ((buf[0] == Resp_STK_INSYNC) && (buf[1] == Resp_STK_OK))
        {
            gFSM = FSM_GET_MAJOR;
            return rv;
        }
        else
        {
            if (g_attempt++ < 6)
            {
                //P1_4 = 0;
                m328p_getsync();
            }
        }
        return rv;
        break;

    case FSM_GET_MAJOR:

        rv = m328p_recv(buf, sizeof(buf));
        if (buf[0] == Resp_STK_NOSYNC)
        {
            P0_1 = 1;// Orange LED.
        }
        if (buf[0] == Resp_STK_INSYNC)
        {
            ELARA_LED1 = 1;
        }
        if (buf[2] == Resp_STK_OK)
        {
            P1_4 = 1;//Yellow LED.
        }
        return rv;
        break;

    case FSM_GET_MINOR:
        break;

    }
    return rv;
}
#endif

// 字符串对比
uint8 str_cmp(uint8 *p1, uint8 *p2, uint8 len)
{
    uint8 i = 0;
    while(i < len)
    {
        if(p1[i] != p2[i])
            return 0;
        i++;
    }
    return 1;
}

extern const uint8 devInfoFirmwareRev[];
bool parse_at_command(uint8 *pBuffer, uint16 length)
{
    bool ret = TRUE;
    char strTemp[64];
    //    uint8 i;
    //    uint8 temp8;
    bool restart = FALSE;

    // 1、测试
    if((length == 4) && str_cmp(pBuffer, "AT\r\n", 4))//AT
    {
        sprintf(strTemp, "OK\r\n");
        HalUARTWrite(0, (uint8 *)strTemp, strlen(strTemp));
    }
#if 0
    // 打印设置的参数  for test only
    else if((length == 8) && str_cmp(pBuffer, "AT+ALL\r\n", 8))//AT
    {
        PrintAllPara();
    }
#endif
    // 2、查询、设置波特率
    else if((length == 10) && str_cmp(pBuffer, "AT+BAUD", 7))
    {
        uint32 baudrate = HAL_UART_BR_57600;
        /*
        发送：AT+BAUD2
        返回：OK+Set:2
        0---------9600
        1---------19200
        2---------38400
        3---------57600
        4---------115200
        */
        switch(pBuffer[7])
        {
        case '?':  //查询当前波特率
            elara_snv_bond_read(&baudrate);
            sprintf(strTemp, "OK+Get:%ld\r\n", baudrate);
            HalUARTWrite(0, (uint8 *)strTemp, strlen(strTemp));
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':  //查询设置新的波特率
            //osal_stop_timerEx(keyfobapp_TaskID, KFD_UART_EVT);
            baudrate = pBuffer[7] - '0';
            elara_snv_bond_write(&baudrate);
            sprintf(strTemp, "OK+Set:%ld\r\n", baudrate);
            HalUARTWrite(0, (uint8 *)strTemp, strlen(strTemp));
            //SerialApp_Init(keyfobapp_TaskID, 1, baudrate);
            //osal_start_timerEx( keyfobapp_TaskID, KFD_TEST_EVT, 1500 );
            restart = TRUE;  //直接重启即可
            
            break;
        default:
            ret = FALSE;
            break;
        }
    }
    //8. 模块复位，重启(Reset)
    else if((length == 10) && str_cmp(pBuffer, "AT+RESET", 8))
    {
        restart = TRUE;  //直接重启即可
    }
    // 17、 查询软件版本
    else if((length == 10) && str_cmp(pBuffer, "AT+VERS?", 8))
    {
        sprintf(strTemp, "%s\r\n", devInfoFirmwareRev);
        HalUARTWrite(0, (uint8 *)strTemp, osal_strlen(strTemp));
    }
#if 0
    // 3、设置串口校验
    else if((length == 10) && str_cmp(pBuffer, "AT+PARI", 7))
    {
        /*
        Para 范围 0,1,2
        0: 无校验
        1: EVEN
        2: ODD
        Default: 0
        */
        switch(pBuffer[7])
        {
        case '?':
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.parity);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
        case '2':
            sys_config.parity = pBuffer[7] - '0';
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.parity);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:
            ret = FALSE;
            break;
        }
    }
    // 4、设置停止位
    else if((length == 10) && str_cmp(pBuffer, "AT+STOP", 7))
    {
        /*
        Para: 0~1
        0: 1 停止位
        1: 2 停止位
        Default: 0
        */
        switch(pBuffer[7])
        {
        case '?':
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.stopbit);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
            sys_config.stopbit = pBuffer[7] - '0';
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.stopbit);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:
            ret = FALSE;
            break;
        }
    }
    // 5. 设置模块工作模式
    else if((length == 10) && str_cmp(pBuffer, "AT+MODE", 7))
    {
        /*
        Para: 0 ~ 1
        0: 开启串口透传模式
        1: 关闭串口透传模式
        2: iBeacon 广播模式
        Default: 0
        */
        switch(pBuffer[7])
        {
        case '?':
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.mode);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
        case '2':
            sys_config.mode = pBuffer[7] - '0';
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.mode);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:
            ret = FALSE;
            break;
        }
    }
    // 6、查询、设置设备名称
    else if((length >= 10 && length <= 20) && str_cmp(pBuffer, "AT+NAME", 7))
    {
        /*
        Para1：设备名称
        最长 11 位数字或字母，
        含中划线和下划线，不建
        议用其它字符。
        Default：DEV_NAME_DEFAULT(见宏定义)
        */
        //int nameLen = length - 7;

        switch(pBuffer[7])
        {
        case '?':
            sprintf(strTemp, "OK+Get:%s\r\n", sys_config.name);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            break;
        default:
            osal_memset(sys_config.name, 0, sizeof(sys_config.name));
            osal_memcpy(sys_config.name, pBuffer + 7, length - 7);
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%s\r\n", sys_config.name);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        }
    }
    //7. 恢复默认设置(Renew)
    else if((length == 10) && str_cmp(pBuffer, "AT+RENEW", 8))
    {
        sprintf(strTemp, "OK+RENEW\r\n");
        NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

        simpleBLE_SetAllParaDefault(PARA_ALL_FACTORY);

        restart = TRUE;  //直接重启即可
    }
    // 9、查询、设置主从模式
    else if((length == 10) && str_cmp(pBuffer, "AT+ROLE", 7))
    {
        /*
        Para1: 0 ~ 1
        1: 主设备
        0: 从设备
        Default: 0
        */
        switch(pBuffer[7])
        {
        case '?':
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.role);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
            temp8 = pBuffer[7] - '0';
            if(temp8 == 0)
            {
                sys_config.role = BLE_ROLE_PERIPHERAL;
            }
            else
            {
                sys_config.role = BLE_ROLE_CENTRAL;
            }
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.role);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:
            ret = FALSE;
            break;
        }
    }
    // 10、 查询、设置配对密码
    else if(((length == 10) && str_cmp(pBuffer, "AT+PASS?", 8))
            || ((length == 15) && str_cmp(pBuffer, "AT+PASS", 7)))
    {
        /*
        Para1: 000000~999999
        Default：000000
        */
        switch(pBuffer[7])
        {
        case '?':
            sprintf(strTemp, "OK+PASS:%s\r\n", sys_config.pass);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            break;
        default:
            osal_memcpy(sys_config.pass, pBuffer + 7, 6);
            sys_config.pass[6] = 0;
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%s\r\n", sys_config.pass);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            break;
        }
    }
    // 11、 设置模块鉴权工作类型
    else if((length == 10) && str_cmp(pBuffer, "AT+TYPE", 7))
    {
        /*
        Para: 0 ~ 1
        0: 连接不需要密码
        1: 连接需要密码
        Default: 0
        */
        switch(pBuffer[7])
        {
        case '?':
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.type);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            break;
        case '0':
        case '1':
            sys_config.type = pBuffer[7] - '0';
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.type);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

            restart = TRUE;  //直接重启即可
            break;
        default:
            ret = FALSE;
            break;
        }
    }
    // 12、 查询本机 MAC 地址
    else if((length >= 10) && str_cmp(pBuffer, "AT+ADDR?", 8))
    {
        sprintf(strTemp, "OK+LADD:%s\r\n", sys_config.mac_addr);
        NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
    }
    // 13、 连接最后一次连接成功的从设备
    else if((length == 10) && str_cmp(pBuffer, "AT+CONNL", 8))
    {
        /*
        Para: L, N, E,F
        L:连接中、N:空地址
        E:连接错误、F:连接失败
        */
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {
            uint8 para[4] = {'L', 'N', 'E', 'F'};
            int8 id = 0;

            g_Central_connect_cmd  = BLE_CENTRAL_CONNECT_CMD_CONNL;

            if(sys_config.ever_connect_peripheral_mac_addr_conut == 0)
            {
                id = 1;
            }
            else
            {
                id = 0;

                // 开始扫描
                {
                    extern void simpleBLEStartScan();
                    simpleBLEStartScan();
                }
            }

            sprintf(strTemp, "AT+CONN%c\r\n", para[id]);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
        }
        else
        {
            ret = FALSE;
        }
    }
    // 14、连接指定蓝牙地址的主设备或从设备
    else if((length == 20) && str_cmp(pBuffer, "AT+CON", 6))
    {
        /*
        Para1: MAC地址、
        如: 0017EA0923AE
        Para2: A, E, F
        A: 连接中
        E: 连接错误
        F: 连接失败
        */
        uint8 para[3] = {'A', 'E', 'F'};
        uint8 id = 0;

        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {
            g_Central_connect_cmd  = BLE_CENTRAL_CONNECT_CMD_CON;
            osal_memcpy(sys_config.connect_mac_addr, pBuffer + 6, MAC_ADDR_CHAR_LEN);

            sprintf(strTemp, "AT+CONN%c\r\n", para[id]);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

            // 开始扫描
            {
                extern void simpleBLEStartScan();
                simpleBLEStartScan();
            }
        }
        else
        {
            ret = FALSE;
        }
    }
    // 15、 清除主设备配对信息
    else if((length == 10) && str_cmp(pBuffer, "AT+CLEAR", 8))
    {
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {
            simpleBLE_SetAllParaDefault(PARA_PARI_FACTORY);
            //PrintAllPara();

            // 下面这个事 系统 API 函数
            GAPBondMgr_SetParameter( GAPBOND_ERASE_ALLBONDS, 0, NULL );

            sprintf(strTemp, "OK+CLEAR\r\n");
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
        }
        else
        {
            ret = FALSE;
        }
    }
    // 16、查询成功连接过的从机地址
    // 备注：此指令只有在主设备时才有效；从设备时不接受此指令
    else if((length == 10) && str_cmp(pBuffer, "AT+RADD?", 8)
            && (BLE_ROLE_CENTRAL == GetBleRole()))//仅主机有效
    {
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {
            if(sys_config.ever_connect_peripheral_mac_addr_conut == 0)//无地址
            {
                sprintf(strTemp, "OK+RADD:NULL\r\n");
                NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            }
            else
            {
                for(i = 0; i < sys_config.ever_connect_peripheral_mac_addr_conut; i++)
                {
                    sprintf(strTemp, "OK+RADD:%s\r\n", sys_config.ever_connect_mac_status[i]);
                    NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
                }
            }
        }
        else
        {
            ret = FALSE;
        }
    }
    // 18、 设置主模式下尝试连接时间
    else if((length == 10) && str_cmp(pBuffer, "AT+TCON", 7))
    {
        /*
        指令	                应答	            参数
        查询：AT+TCON?	        OK+TCON:[para]
        设置：AT+TCON[para]	    OK+Set:[para] 	    Para: 000000～009999
                                                    000000 代表持续连接，其
                                                    余代表尝试的毫秒数
                                                    Default:001000
        */
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {
            if(pBuffer[7] == '?')
            {
                sprintf(strTemp, "%06d\r\n", sys_config.try_connect_time_ms);
                NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            }
            else
            {
                sys_config.try_connect_time_ms = 10000;//_atoi(pBuffer+7);
                simpleBLE_WriteAllDataToFlash();
                sprintf(strTemp, "OK+Set:%06d\r\n", sys_config.try_connect_time_ms);
                NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            }
        }
        else
        {
            ret = FALSE;
        }
    }
    // 19、 读取 RSSI 信号值
    else if((length == 10) && str_cmp(pBuffer, "AT+RSSI?", 10))
    {
        sprintf(strTemp, "OK+RSSI:%d\r\n", sys_config.rssi);
        NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
    }
    // 20、  改变模块发射信号强度
    else if((length == 10) && str_cmp(pBuffer, "AT+TXPW", 7))
    {
        /*
        指令	        应答	            参数
        查询：          AT+TXPW?	        OK+ TXPW:[para]
        设置：          AT+TXPW[para]	    OK+Set:[para]	Para: 0 ~ 3
                                            0: 4dbm、1: 0dbm
                                            2: -6dbm、3: -23dbm
                                            Default: 0
        */
        if(pBuffer[7] == '?')
        {
            sprintf(strTemp, "AT+TXPW:%d\r\n", sys_config.txPower);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
        }
        else
        {
            sys_config.txPower = pBuffer[7] - '0';
            if(sys_config.txPower > 3)
                sys_config.txPower = 0;
            simpleBLE_WriteAllDataToFlash();
            sprintf(strTemp, "OK+Set:%d\r\n", sys_config.txPower);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

#if 1
            /*
            #define LL_EXT_TX_POWER_MINUS_23_DBM                   0
            #define LL_EXT_TX_POWER_MINUS_6_DBM                    1
            #define LL_EXT_TX_POWER_0_DBM                          2
            #define LL_EXT_TX_POWER_4_DBM                          3
            */
            // HCI_EXT_SetTxPowerCmd()是用来设置发射功率的. 有-23dbm, -6dbm, 0 dbm, +4dbm四个级别.
            HCI_EXT_SetTxPowerCmd(3 - sys_config.txPower);
#endif

            restart = TRUE;  //直接重启即可
        }
    }
    // 21、  改变模块作为ibeacon基站广播时间间隔
    else if((length == 10 || length == 15) && str_cmp(pBuffer, "AT+TIBE", 7))
    {
        /*
        指令	        应答	        参数
        查询：          AT+TIBE?	    OK+ TIBE:[para]
        设置：          AT+TIBE[para]	OK+Set:[para]	Para: 000000～009999
                                        000000 代表持续广播，其
                                        余代表尝试的毫秒数
                                        Default:000500
        */
        if((GetBleRole() == BLE_ROLE_PERIPHERAL))
        {
            if(pBuffer[7] == '?')
            {
                sprintf(strTemp, "AT+TIBE:%06d\r\n", sys_config.ibeacon_adver_time_ms);
                NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            }
            else
            {
                sys_config.ibeacon_adver_time_ms = str2Num(pBuffer + 7, 6);
                if(sys_config.ibeacon_adver_time_ms <= 9999)
                {
                    simpleBLE_WriteAllDataToFlash();
                    sprintf(strTemp, "OK+Set:%06d\r\n", sys_config.ibeacon_adver_time_ms);
                    NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

                    restart = TRUE;  //直接重启即可
                }
            }
        }
        else
        {
            ret = FALSE;
        }
    }
    // 22、  设置模块工作类型
    else if((length == 10) && str_cmp(pBuffer, "AT+IMME", 7))
    {
        /*
        指令	        应答	        参数
        查询：          AT+IMME?	    OK+Get:[para]	Para: 0~1
        设置：          AT+IMME[para]	OK+Set:[para]	Para: 0~1
                                        000000 代表持续广播，其
                                        0: 立即工作，
                                        1: 等待AT+CON 或 AT+CONNL 命令
                                        Default:0
        */
        if(pBuffer[7] == '?')
        {
            sprintf(strTemp, "OK+Get:%d\r\n", sys_config.workMode);
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
        }
        else
        {
            sys_config.workMode = str2Num(pBuffer + 7, 1);
            if(sys_config.workMode <= 1)
            {
                simpleBLE_WriteAllDataToFlash();
                sprintf(strTemp, "OK+Set:%d\r\n", sys_config.workMode);
                NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));
            }

            restart = TRUE;  //直接重启即可
        }
    }
    // 23、 设置开始主模式下的从机扫描
    else if((length == 10) && str_cmp(pBuffer, "AT+DISC?\r\n", 10))
    {
        /*
        AT+DISC? OK+DISCS 无
        该指令为主模式、手动工作模式指令。使用前须满足 AT+ROLE1，AT+IMME1
        条件。
        发送 AT+DISC?指令后，模块若处于待机状态，返回 OK+DISCS 后开始搜索。
        如果搜到 BLE 设备会返回 OK+DISC:123456789012,其中数字字符串代表对
        方蓝牙地址，最多返回 6 个，下标从 0 开始。搜索完成后返回 OK+DISCE。
        如只搜到二个设备的例子：
        发送: AT+DISC?
        接收: OK+DISCS
        接收: OK+DISC:123456789012
        接收: OK+DISCE
        如果要连接搜到的设备，下标从 0 开始。
        发送: AT+CONN0 连接搜到的第一个设备
        发送: AT+CONN1 连接搜到的第二个设备
        依此类推
        */
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {
            g_Central_connect_cmd  = BLE_CENTRAL_CONNECT_CMD_DISC;

            sprintf(strTemp, "OK+DISCS\r\n");
            NPI_WriteTransport((uint8 *)strTemp, osal_strlen(strTemp));

            // 开始扫描
            {
                extern void simpleBLEStartScan();
                simpleBLEStartScan();
            }
        }
        else
        {
            ret = FALSE;
        }
    }
    // 24、 连接已经发现的从机， 输入的是下标号
    else if((length == 10) && str_cmp(pBuffer, "AT+CONN", 7))
    {
        /*
        发送: AT+CONN0 连接搜到的第一个设备
        发送: AT+CONN1 连接搜到的第二个设备
        依此类推
        */
        if((GetBleRole() == BLE_ROLE_CENTRAL))
        {
            //sprintf(strTemp, "OK+DISCS\r\n");
            //NPI_WriteTransport((uint8*)strTemp, osal_strlen(strTemp));

            g_Central_connect_cmd  = BLE_CENTRAL_CONNECT_CMD_CONN;

            temp8 = pBuffer[7] - '0';
            // 开始扫描
            {
                extern bool simpleBLEConnect(uint8 index);
                simpleBLEConnect(temp8);
            }
        }
        else
        {
            ret = FALSE;
        }
    }
#endif
    else
    {
        ret = FALSE;
    }
#if 1
    if (restart)//如果该标志已设置，稍微延时后重启
    {
        //simpleBLE_Delay_1ms(200);      //设置参数后，适当延时， 以便上一次发送的数据正常发送出去
        KFD_HAL_DELAY(25000);// 200ms
        HAL_SYSTEM_RESET();
    }
#endif
    return ret;
}

extern int test_spi_get_status_register(uint8 *pBuf, uint8 len);
extern int test_spi_get_status_register1(uint8 *pBuf, uint8 len);
extern int test_spi_get_status_register2(uint8 *pBuf, uint8 len);
extern int HalSPIEraseChip(void);
extern int arduino_open_for_signature(void);
extern int arduino_read_signature(unsigned char *sig);
#if 0
static void getBaudAndSignature(void)
{
    int rc_open = -1;
    int rc_read = -1;
    int rc_open1 = -1;
    int rc_read1 = -1;
    uint8 value[3];
    uint8 m644pSignature[3] = {0x1e, 0x96, 0x0a};
    uint8 m328pSignature[3] = {0x1e, 0x95, 0x0f};
    uint8 m1284pSignature[3] = {0x1e, 0x97, 0x05};
    uint8 m128rfa1Signature[3] = {0x1e, 0xa7, 0x01};
#if 1
    gElaraBaud = HAL_UART_BR_115200;
    SerialApp_Init(keyfobapp_TaskID, 0, gElaraBaud);
    rc_open = arduino_open_for_signature();
    rc_read = arduino_read_signature(value);
    rc_open1 = -1;
    rc_read1 = -1;
    
    if (memcmp(value, m328pSignature, 3) == 0)
    {
        gCurPartID = m328p_16M_5V;
        goto SigExit;
    }
    else if (memcmp(value, m644pSignature, 3) == 0)
    {
        gCurPartID = m644p_16M_5V;
        goto SigExit;
    }
    else if (memcmp(value, m1284pSignature, 3) == 0)
    {
        gCurPartID = m1284p_16M_5V;
        goto SigExit;
    }
    else if (memcmp(value, m128rfa1Signature, 3) == 0)
    {
        gCurPartID = m128rfa1_16M;
        goto SigExit;
    }
#endif
    // If goto here, baudrate should be HAL_UART_BR_57600
    gElaraBaud = HAL_UART_BR_57600;
    KFD_HAL_DELAY(25000);// 200ms
    SerialApp_Init(keyfobapp_TaskID, 0, gElaraBaud);
    KFD_HAL_DELAY(30000);// 200ms
    rc_open1 = arduino_open_for_signature();
    rc_read1 = arduino_read_signature(value);
    if (memcmp(value, m328pSignature, 3) == 0)
    {
        gCurPartID = m328p_8M_3_3V;
        goto SigExit;
    }
    else if (memcmp(value, m644pSignature, 3) == 0)
    {
        gCurPartID = m644p_8M_3_3V;
        goto SigExit;
    }
    else if (memcmp(value, m1284pSignature, 3) == 0)
    {
        gCurPartID = m1284p_8M_3_3V;
        goto SigExit;
    }
    else if (memcmp(value, m128rfa1Signature, 3) == 0)
    {
        gCurPartID = m128rfa1_16M;
        goto SigExit;
    }
    // If goto here, we can not get the core type.
    gCurPartID = 0xFF;
SigExit:
#if 0
    advertDataBeacon_OLD[9] = value[0];
    advertDataBeacon_OLD[10] = value[1];
    advertDataBeacon_OLD[11] = value[2];
    advertDataBeacon_OLD[12] = rc_open;
    advertDataBeacon_OLD[13] = rc_read;
    advertDataBeacon_OLD[14] = rc_open1;
    advertDataBeacon_OLD[15] = rc_read1;
    GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon_OLD ), advertDataBeacon_OLD );
#endif

    uint8 realPartID = gCurPartID;
    MDProfile_SetParameter(MDPROFILE_PARTID, 1, &realPartID);
    osal_start_timerEx( keyfobapp_TaskID, KFD_LED_BLINK_EVT, 102 );
}
#else
static void getBaudAndSignature(void)// 57600 first.
{
    int rc_open = -1;
    int rc_read = -1;
    int rc_open1 = -1;
    int rc_read1 = -1;
    uint8 value[3] = {0,0,0};
    uint8 m644pSignature[3] = {0x1e, 0x96, 0x0a};
    uint8 m328pSignature[3] = {0x1e, 0x95, 0x0f};
    uint8 m1284pSignature[3] = {0x1e, 0x97, 0x05};
    uint8 m128rfa1Signature[3] = {0x1e, 0xa7, 0x01};

    gElaraBaud = HAL_UART_BR_57600;
    if (gUAS == UPDATE_AVR_STATE_6)
    {
        SerialApp_Init_2(keyfobapp_TaskID, 0, gElaraBaud);
    }
    else
    {
        SerialApp_Init(keyfobapp_TaskID, 0, gElaraBaud);
    }
    rc_open = arduino_open_for_signature();
    rc_read = arduino_read_signature(value);
    rc_open1 = -1;
    rc_read1 = -1;
    
    if (memcmp(value, m328pSignature, 3) == 0)
    {
        gCurPartID = m328p_8M_3_3V;
        goto SigExit;
    }
    else if (memcmp(value, m644pSignature, 3) == 0)
    {
        gCurPartID = m644p_8M_3_3V;
        goto SigExit;
    }
    else if (memcmp(value, m1284pSignature, 3) == 0)
    {
        gCurPartID = m1284p_8M_3_3V;
        goto SigExit;
    }
    else if (memcmp(value, m128rfa1Signature, 3) == 0)
    {
        gCurPartID = m128rfa1_16M;
        goto SigExit;
    }
#if 1
    // If goto here, baudrate should be HAL_UART_BR_115200
    gElaraBaud = HAL_UART_BR_115200;
    KFD_HAL_DELAY(25000);// 200ms
    if (gUAS == UPDATE_AVR_STATE_6)
    {
        SerialApp_Init_2(keyfobapp_TaskID, 0, gElaraBaud);
    }
    else
    {
        SerialApp_Init_2(keyfobapp_TaskID, 0, gElaraBaud);
    }
    KFD_HAL_DELAY(30000);// 240ms
    rc_open1 = arduino_open_for_signature();
    rc_read1 = arduino_read_signature(value);
    if (memcmp(value, m328pSignature, 3) == 0)
    {
        gCurPartID = m328p_16M_5V;
        goto SigExit;
    }
    else if (memcmp(value, m644pSignature, 3) == 0)
    {
        gCurPartID = m644p_16M_5V;
        goto SigExit;
    }
    else if (memcmp(value, m1284pSignature, 3) == 0)
    {
        gCurPartID = m1284p_16M_5V;
        goto SigExit;
    }
#endif
    // If goto here, we can not get the core type.
    gCurPartID = 0xFF;
    uint8 realPartID;
SigExit:
#if 0
    advertDataBeacon_OLD[9] = value[0];
    advertDataBeacon_OLD[10] = value[1];
    advertDataBeacon_OLD[11] = value[2];
    advertDataBeacon_OLD[12] = rc_open;
    advertDataBeacon_OLD[13] = rc_read;
    advertDataBeacon_OLD[14] = rc_open1;
    advertDataBeacon_OLD[15] = rc_read1;
    GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon_OLD ), advertDataBeacon_OLD );
#endif

    realPartID = gCurPartID;
    MDProfile_SetParameter(MDPROFILE_PARTID, 1, &realPartID);
    //osal_start_timerEx( keyfobapp_TaskID, KFD_LED_BLINK_EVT, 102 );
}
#endif

static void processAVRevent(void)
{
    FlashType ft = FlashType_w25q;
    //FlashType ft = FlashType_CC2541;
    elara_flash_common_init(ft);// To use SPI FLASH or CC2541 internal FLASH.
    uint32 uState;
    if (ft == FlashType_w25q)
    {
        uState = 3;
    }
    else
    {
        uState = 0;
    }
    uint8 ret = elara_snv_read(BLE_NVID_U_STATE, 1, &uState);

    if (uState == 0)//In this state, CC2541 wait phone handset to connect to download bin
    {
#if 0
        //MDMCTRL0 = 0x04;
        //MDMCTRL0 = 0x06;
        //MDMCTRL0 = 0x0E;
        ELARA_LED1 = 1;
        elara_flash_any_thing_else();
        uint16 of = 0;
        uint32 binSize;
        binSize = elara_flash_if_flash_enough(282);
        uint32 rc;
        //osal_snv_read(BLE_NVID_MCU_BIN_SIZE, 4, &binSize);

        //rc = sizeof(t_part_id);
        rc = binSize;
        advertDataBeacon[27] = (rc >> 24) & 0xFF;
        advertDataBeacon[28] = (rc >> 16) & 0xFF;
        advertDataBeacon[29] = (rc >> 8) & 0xFF;
        advertDataBeacon[30] = rc & 0xFF;

        //osal_snv_read(BLE_NVID_ACTIVE_OFFSET, 2, &of);
        binSize = 0;
        osal_snv_read(BLE_NVID_ADR_ACTIVE, 4, &binSize);
        rc = binSize;
        advertDataBeacon[23] = (rc >> 24) & 0xFF;
        advertDataBeacon[24] = (rc >> 16) & 0xFF;
        advertDataBeacon[25] = (rc >> 8) & 0xFF;
        advertDataBeacon[26] = rc & 0xFF;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
        //osal_start_timerEx( keyfobapp_TaskID, KFD_JUST_FOR_TEST1_FLASH_EVT, 60000 );
#else
        ELARA_LED1 = 1;
        elara_flash_write_init();
#endif
        osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
        if (gUAS != UPDATE_AVR_STATE_6)
        {
            getBaudAndSignature();
        }
        gManualTerminate = 0;
        gUAS = UPDATE_AVR_IDLE;

        #if 0
        if (gElaraBaud != baudrate)//串口透传的波特率固定为57600
        {
            SerialApp_Init(keyfobapp_TaskID, 0, baudrate);
        }
        osal_start_timerEx( keyfobapp_TaskID, KFD_UART_EVT, 900 );
        #else
    #ifdef ELARA_UNINIT_UART
        osal_start_timerEx( keyfobapp_TaskID, KFD_UART_UNINIT_EVT, 100 );
    #else
        osal_start_timerEx( keyfobapp_TaskID, KFD_UART_INIT_EVT, 1000 );
    #endif
        #endif
        #if 0
        uint8 tempBuffer[50] = {0};
        sprintf((char *)tempBuffer, "baud rate=%ld\r\n", baudrate);
        HalUARTWrite (0, tempBuffer, strlen((char const *)tempBuffer));
        #endif
        #if 0
        uint8 drainBuf[64];
        HalUARTRead (0, drainBuf, sizeof(drainBuf));
        #endif
#if 0
        uint32 AddressBegin = 23;
        uint32 bin_size = 23;
        uint32 AddressActive = 23;
        elara_snv_read(BLE_NVID_ADR_ACTIVE, 4, &AddressActive);
        elara_snv_read(BLE_NVID_ADR_BEGIN, 4, &AddressBegin);
        elara_snv_read(BLE_NVID_MCU_BIN_SIZE, 4, &bin_size);
        advertDataBeacon[19] = (AddressBegin >> 24) & 0xFF;
        advertDataBeacon[20] = (AddressBegin >> 16) & 0xFF;
        advertDataBeacon[21] = (AddressBegin >> 8) & 0xFF;
        advertDataBeacon[22] = AddressBegin & 0xFF;
        advertDataBeacon[23] = (bin_size >> 24) & 0xFF;
        advertDataBeacon[24] = (bin_size >> 16) & 0xFF;
        advertDataBeacon[25] = (bin_size >> 8) & 0xFF;
        advertDataBeacon[26] = bin_size & 0xFF;
        advertDataBeacon[27] = (AddressActive >> 24) & 0xFF;
        advertDataBeacon[28] = (AddressActive >> 16) & 0xFF;
        advertDataBeacon[29] = (AddressActive >> 8) & 0xFF;
        advertDataBeacon[30] = AddressActive & 0xFF;
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
#endif
        return;
    }
    if (uState == 3)
    {
//        uint8 adv = FALSE;
//        GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &adv );
        osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
        #if 1
        if (elara_flash_if_first_256_FF() == TRUE)
        {
            uState = 0;
            elara_snv_write(BLE_NVID_U_STATE, 1, &uState);
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_NOW, 1000 );
            return;
        }
        #endif
#if 1
        int rc = 1;
        rc = elara_flash_erase();
        //rc = HalSPIEraseChip();
        //HalSPIEraseBlockInit();
        if (rc > 0)
        {
            HalLedSet(HAL_LED_2, HAL_LED_MODE_OFF );
            ELARA_LED1 = 0;
            ELARA_LED2 = 0;
            osal_start_timerEx( keyfobapp_TaskID, KFD_ERASE_EVT, 1000 );
        }
        else
        {
            uState = 0;
            osal_snv_write(BLE_NVID_U_STATE, 1, &uState);
            osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_NOW, 4001 );
        }
#else
        uint8 buffer[8] = {0x76, 0x76, 0x76, 0x76, 0x76, 0x76, 0x76, 0x76};
        HalLedSet(HAL_LED_2, HAL_LED_MODE_OFF );
        test_spi_get_status_register2(buffer, 8);
        //test_spi_get_status_register1(buffer, 4);
        advertDataBeacon[23] = buffer[0];
        advertDataBeacon[24] = buffer[1];
        advertDataBeacon[25] = buffer[2];
        advertDataBeacon[26] = buffer[3];
        advertDataBeacon[27] = buffer[4];
        advertDataBeacon[28] = buffer[5];
        advertDataBeacon[29] = buffer[6];
        advertDataBeacon[30] = buffer[7];
        GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
#endif
        return;
    }
#if 0
    if (uState == 2) //Just for test
    {
        osal_start_timerEx( keyfobapp_TaskID, KFD_JUST_FOR_TEST_FLASH_EVT, 100 );
        return;// (events ^ KFD_INIT_1_AVR_EVT);
    }
#endif
#if defined(DEBUG_SERIAL)
    osal_start_timerEx( keyfobapp_TaskID, KFD_TEST_EVT, 6000 );
    uint16 usedmem = 0;
    usedmem = osal_heap_mem_used();
    SerialPrintValue("BLE used memory is : ", usedmem, 10);
#endif
    //uint8 adv = FALSE;
    //GAPRole_SetParameter( GAPROLE_ADVERT_ENABLED, sizeof( uint8 ), &adv );
    osal_pwrmgr_device( PWRMGR_ALWAYS_ON );

    SerialPrintString("Begin avr_init()\r\n");
    int rc;
    uint32 partID = m1284p_16M_5V;
    elara_snv_read(BLE_NVID_PART_ID, 1, &partID);
    if (partID == m328p_16M_5V || partID == m644p_16M_5V || partID == m1284p_16M_5V ||
            partID == m328_16M_5V || partID == m644_16M_5V || partID == m1284_16M_5V)
    {
        SerialApp_Init(keyfobapp_TaskID, 0, HAL_UART_BR_115200);
    }
    else
    {
        SerialApp_Init(keyfobapp_TaskID, 0, HAL_UART_BR_57600);
    }
    switch (partID)
    {
    case m328_16M_5V:
    case m328_8M_3_3V:
        rc = avr_init_common("m328");
        break;
    case m328p_16M_5V:
    case m328p_8M_3_3V:
        rc = avr_init_common("m328p");
        break;
    case m644_16M_5V:
    case m644_8M_3_3V:
        rc = avr_init_common("m644");
        break;
    case m644p_16M_5V:
    case m644p_8M_3_3V:
        rc = avr_init_common("m644p");
        break;
    case m1284_16M_5V:
    case m1284_8M_3_3V:
        rc = avr_init_common("m1284");
        break;
    case m1284p_16M_5V:
    case m1284p_8M_3_3V:
        rc = avr_init_common("m1284p");
        break;
    case m128rfa1_16M:
        rc = avr_init_common("m128rfa1");
        break;
    case m32u4_16M:
        rc = avr_init_common("m32u4");
        break;

    case m128rfr2:
        rc = avr_init_common("m128rfr2");
        break;
    case m64rfr2:
        rc = avr_init_common("m64rfr2");
        break;
    case m2560:
        rc = avr_init_common("m2560");
        break;
    case m2561:
        rc = avr_init_common("m2561");
        break;
    }
#if 0
    rc = -rc;
    advertDataBeacon[25] = (rc >> 8) & 0xFF;
    advertDataBeacon[26] = rc & 0xFF;
    GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
    //ELARA_LED1 = 1;
#endif
    SerialPrintString("Finish avr_init()\r\n");
#if defined(DEBUG_SERIAL)
    usedmem = osal_heap_mem_used();
    SerialPrintValue("Now BLE used memory is : ", usedmem, 10);
#endif
    //After we had upgrade avr firmware, we should reset CC2541 to goto normal state.
#if 1
    uState = 0;
    elara_snv_write(BLE_NVID_U_STATE, 1, &uState);
    osal_start_timerEx( keyfobapp_TaskID, KFD_RESET_NOW, 5000 );
#else
    uint32 AddressBegin = 0;
    uint32 bin_size = 0;
    elara_snv_read(BLE_NVID_ADR_BEGIN, 4, &AddressBegin);
    elara_snv_read(BLE_NVID_MCU_BIN_SIZE, 4, &bin_size);
    advertDataBeacon[19] = (AddressBegin >> 24) & 0xFF;
    advertDataBeacon[20] = (AddressBegin >> 16) & 0xFF;
    advertDataBeacon[21] = (AddressBegin >> 8) & 0xFF;
    advertDataBeacon[22] = AddressBegin & 0xFF;
    advertDataBeacon[23] = (bin_size >> 24) & 0xFF;
    advertDataBeacon[24] = (bin_size >> 16) & 0xFF;
    advertDataBeacon[25] = (bin_size >> 8) & 0xFF;
    advertDataBeacon[26] = bin_size & 0xFF;
    advertDataBeacon[27] = (partID >> 24) & 0xFF;
    advertDataBeacon[28] = (partID >> 16) & 0xFF;
    advertDataBeacon[29] = (partID >> 8) & 0xFF;
    advertDataBeacon[30] = partID & 0xFF;
    GAPRole_SetParameter( GAPROLE_ADVERT_DATA, sizeof( advertDataBeacon ), advertDataBeacon );
#endif
}

extern void avr_write_elara_2_finished(void);
extern int avr_write_elara_2_next(void);
static void UpdateAVRevent(void)
{
    int exitRC = 0;
    if (gUAS == UPDATE_AVR_CONN_PARAM_UPDATE)
    {
        ELARA_LED1 = 0;
        ELARA_LED2 = 0;
        osal_start_timerEx( keyfobapp_TaskID, KFD_CONN_PARAM_UPDATE_EVT, 100);
        gUAS = UPDATE_AVR_INIT_ELARA_SNV;
        osal_start_timerEx( keyfobapp_TaskID, KFD_UPDATE_AVR_EVT, 5000 );
    }
    else if (gUAS == UPDATE_AVR_INIT_ELARA_SNV)
    {
        elara_snv_init();
        osal_start_timerEx( keyfobapp_TaskID, KFD_UPDATE_AVR_EVT, 1000 );
        gUAS = UPDATE_AVR_INIT_AVR;
    }
    else if (gUAS == UPDATE_AVR_INIT_AVR)
    {
        osal_pwrmgr_device( PWRMGR_ALWAYS_ON );

        int rc;
        uint32 partID = m1284p_16M_5V;
        elara_snv_read(BLE_NVID_PART_ID, 1, &partID);
        if (partID == m328p_16M_5V || partID == m644p_16M_5V || partID == m1284p_16M_5V ||
                partID == m328_16M_5V || partID == m644_16M_5V || partID == m1284_16M_5V)
        {
            #ifdef ELARA_UNINIT_UART
            SerialApp_Init(keyfobapp_TaskID, 0, HAL_UART_BR_115200);
            #else
            SerialApp_Init_2(keyfobapp_TaskID, 0, HAL_UART_BR_115200);
            #endif
        }
        else
        {
            #ifdef ELARA_UNINIT_UART
            SerialApp_Init(keyfobapp_TaskID, 0, HAL_UART_BR_57600);
            #else
            SerialApp_Init_2(keyfobapp_TaskID, 0, HAL_UART_BR_57600);
            #endif
        }
        switch (partID)
        {
        case m328_16M_5V:
        case m328_8M_3_3V:
            rc = avr_init_common_1("m328");
            break;
        case m328p_16M_5V:
        case m328p_8M_3_3V:
            rc = avr_init_common_1("m328p");
            break;
        case m644_16M_5V:
        case m644_8M_3_3V:
            rc = avr_init_common_1("m644");
            break;
        case m644p_16M_5V:
        case m644p_8M_3_3V:
            rc = avr_init_common_1("m644p");
            break;
        case m1284_16M_5V:
        case m1284_8M_3_3V:
            rc = avr_init_common_1("m1284");
            break;
        case m1284p_16M_5V:
        case m1284p_8M_3_3V:
            rc = avr_init_common_1("m1284p");
            break;
        case m128rfa1_16M:
            rc = avr_init_common_1("m128rfa1");
            break;
        case m32u4_16M:
            rc = avr_init_common_1("m32u4");
            break;

        case m128rfr2:
            rc = avr_init_common_1("m128rfr2");
            break;
        case m64rfr2:
            rc = avr_init_common_1("m64rfr2");
            break;
        case m2560:
            rc = avr_init_common_1("m2560");
            break;
        case m2561:
            rc = avr_init_common_1("m2561");
            break;
        default:
            break;
        }
        gUAS = UPDATE_AVR_STATE_2;
        osal_start_timerEx( keyfobapp_TaskID, KFD_UPDATE_AVR_EVT, 500 );
    }
    else if (gUAS == UPDATE_AVR_STATE_2)
    {
        avr_init_common_2();
        gUAS = UPDATE_AVR_STATE_3;
        osal_start_timerEx( keyfobapp_TaskID, KFD_UPDATE_AVR_EVT, 500 );
    }
    else if (gUAS == UPDATE_AVR_STATE_3)
    {
        exitRC = avr_write_elara_2_next();
        if (exitRC == -1)//finished.
        {
            uint32 uState = 0;
            elara_snv_write(BLE_NVID_U_STATE, 1, &uState);
            osal_start_timerEx( keyfobapp_TaskID, KFD_UPDATE_AVR_EVT, 1000 );
            gUAS = UPDATE_AVR_STATE_4;
            return;
        }
        osal_start_timerEx( keyfobapp_TaskID, KFD_UPDATE_AVR_EVT, 10 );
    }
    else if (gUAS == UPDATE_AVR_STATE_4)
    {
        avr_write_elara_2_finished();
        gUAS = UPDATE_AVR_STATE_5;
        osal_start_timerEx( keyfobapp_TaskID, KFD_UPDATE_AVR_EVT, 900 );
    }
    else if (gUAS == UPDATE_AVR_STATE_5)
    {
        #ifdef ELARA_UNINIT_UART
        SerialApp_Uninit();
        #endif
        char strTemp[32] = "Update done";
        MDSerialAppSendNoti((uint8*)strTemp, 11);
        gUAS = UPDATE_AVR_STATE_6;
        osal_start_timerEx( keyfobapp_TaskID, KFD_UPDATE_AVR_EVT, 100 );
    }
    else if (gUAS == UPDATE_AVR_STATE_6)
    {
        elara_snv_init();
        
        osal_start_timerEx( keyfobapp_TaskID, KFD_INIT_1_AVR_EVT, 2000 );
    }
}
