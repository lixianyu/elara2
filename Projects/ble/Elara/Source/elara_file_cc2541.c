/*********************************************************************
 * INCLUDES
 */
#include "bcomdef.h"
#include "hal_adc.h"
#include "hal_flash.h"
#include "hal_types.h"
#include "comdef.h"
#include "OSAL.h"
#include "osal_snv.h"
#include "hal_assert.h"
#include "saddr.h"
#include "elara_error.h"
#include "elara_led.h"

/*********************************************************************
 * CONSTANTS
 */

// NV page configuration
#define OSAL_NV_PAGE_SIZE       HAL_FLASH_PAGE_SIZE
#define OSAL_NV_PAGES_USED      HAL_NV_PAGE_CNT
#define OSAL_NV_PAGE_BEG        HAL_NV_PAGE_BEG
#define OSAL_NV_PAGE_END       (OSAL_NV_PAGE_BEG + OSAL_NV_PAGES_USED - 1)

// Default byte value when flash is erased
#define OSAL_NV_ERASED          0xFF

// NV page header size in bytes
#define OSAL_NV_PAGE_HDR_SIZE  4

// In case pages 0-1 are ever used, define a null page value.
#define OSAL_NV_PAGE_NULL       0

// In case item Id 0 is ever used, define a null item value.
#define OSAL_NV_ITEM_NULL       0

// Length in bytes of a flash word
#define OSAL_NV_WORD_SIZE       HAL_FLASH_WORD_SIZE

// NV page header offset within a page
#define OSAL_NV_PAGE_HDR_OFFSET 0


// Flag in a length field of an item header to indicate validity
// of the length field
#define OSAL_NV_INVALID_LEN_MARK 0x8000

// Flag in an ID field of an item header to indicate validity of
// the identifier field
#define OSAL_NV_INVALID_ID_MARK  0x8000


// Bit difference between active page state indicator value and
// transfer page state indicator value
#define OSAL_NV_ACTIVE_XFER_DIFF  0x00100000

// active page state indicator value
#define OSAL_NV_ACTIVE_PAGE_STATE OSAL_NV_ACTIVE_XFER_DIFF

// transfer page state indicator value
#define OSAL_NV_XFER_PAGE_STATE   (OSAL_NV_ACTIVE_PAGE_STATE ^ OSAL_NV_ACTIVE_XFER_DIFF)

#define OSAL_NV_MIN_COMPACT_THRESHOLD   70 // Minimum compaction threshold
#define OSAL_NV_MAX_COMPACT_THRESHOLD   95 // Maximum compaction threshold

/*********************************************************************
 * MACROS
 */
#define ELARA_PAGE_BEGIN 80
//Should be 124
//#define ELARA_PAGE_END (HAL_NV_PAGE_BEG-1)
#define ELARA_PAGE_END 124
//#define ELARA_PAGE_END 95
/*********************************************************************
 * TYPEDEFS
 */


/*********************************************************************
 * EXTERNAL FUNCTIONS
 */


/*********************************************************************
 * GLOBAL VARIABLES
 */
#if 0
#if 1
#pragma location="ELARA_FLASH_SPACE"
//26624 + 32768 + 32768 = 92160
__no_init uint8 _elaraBuf[32768];
#pragma required=_elaraBuf
#endif
#if 1
#pragma location="ELARA_FLASH_SPACE1"
//26624 + 32768 + 32768 = 92160
__no_init uint8 _elaraBuf1[32768];
#pragma required=_elaraBuf1
#endif
#if 1
#pragma location="ELARA_FLASH_SPACE2"
//26624 + 32768 + 32768 = 92160
__no_init uint8 _elaraBuf2[26624];
#pragma required=_elaraBuf2
#endif
#endif
/*********************************************************************
 * LOCAL VARIABLES
 */
static uint8 g_beginPg = ELARA_PAGE_BEGIN; // The begin page
static uint8 g_activePg = ELARA_PAGE_BEGIN;// Active page (Or call end page)

// active page offset, g_pageBeginOffset%4 must be zero
static uint16 g_pageBeginOffset = 0; // The begin offset
static uint16 g_pageActiveOffset = 0;// The active offset

static uint32 g_bin_size = 0;

// flag to indicate that an error has occurred while writing to or erasing the
// flash device. Once this flag indicates failure, it is unsafe to attempt
// another write or erase.
static uint8 failF;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
int elara_flash_read_init_cc2541(void)
{
    uint8 ret = osal_snv_read(BLE_NVID_MCU_BIN_SIZE, 4, &g_bin_size);
    if (ret != SUCCESS)
    {
        g_bin_size = 0;
    }
    if (g_bin_size == 0)
    {
        return ELARA_ERROR_105;
    }
    ret = osal_snv_read(BLE_NVID_BEGIN_PAGE, 1, &g_beginPg);
    if (ret != SUCCESS)
    {
        g_beginPg = ELARA_PAGE_BEGIN;
    }
    ret = osal_snv_read(BLE_NVID_BEGIN_OFFSET, 2, &g_pageBeginOffset);
    if (ret != SUCCESS)
    {
        g_pageBeginOffset = 0;
    }
    g_activePg = g_beginPg;
    g_pageActiveOffset = g_pageBeginOffset;
    return 0;
}

void elara_flash_write_init_cc2541(void)
{
    uint8 ret;
#if 0
    ret = osal_snv_read(BLE_NVID_BEGIN_PAGE, 1, &g_beginPg);
    if (ret != SUCCESS)
    {
        g_beginPg = ELARA_PAGE_BEGIN;
    }
    ret = osal_snv_read(BLE_NVID_BEGIN_OFFSET, 2, &g_pageBeginOffset);
    if (ret != SUCCESS)
    {
        g_pageBeginOffset = 0;
    }
#endif
    ret = osal_snv_read(BLE_NVID_ACTIVE_PAGE, 1, &g_activePg);
    if (ret != SUCCESS)
    {
        g_activePg = ELARA_PAGE_BEGIN;
    }
    ret = osal_snv_read(BLE_NVID_ACTIVE_OFFSET, 2, &g_pageActiveOffset);
    if (ret != SUCCESS)
    {
        g_pageActiveOffset = 0;
    }
    g_beginPg = g_activePg;
    g_pageBeginOffset = g_pageActiveOffset;
    g_bin_size = 0;
}

/*********************************************************************
 * @fn      elara_flash_if_flash_enough
 *
 * @brief   Before write bin to flash, we should check flash memory big enough.
            (Should run elara_flash_write_init function first.)
 *
 * @param   binSize - The size of the bin
 *
 * @return  The aviliabel flash size, 0 fail.
 */
uint32 elara_flash_if_flash_enough_cc2541(uint32 binSize)
{
    uint32 allFlashSize;

    uint32 pcont = ELARA_PAGE_END - g_activePg;

    // The aviliable flash size now.
    uint32 flashsizeAV = (uint32)(pcont * (uint32)HAL_FLASH_PAGE_SIZE);// + (uint32)(HAL_FLASH_PAGE_SIZE - g_pageActiveOffset);
    flashsizeAV += ((uint32)HAL_FLASH_PAGE_SIZE - g_pageActiveOffset);
    // 4 bytes align
    uint32 real = ((binSize + (uint32)HAL_FLASH_WORD_SIZE - 1) / (uint32)HAL_FLASH_WORD_SIZE) * (uint32)HAL_FLASH_WORD_SIZE;//4

    if (ELARA_PAGE_END < g_activePg)
    {
        goto enoughExits;
    }
#if 0
    if (pcont == 0 && g_pageActiveOffset > 0)
    {
        goto enoughExits;
    }
#endif
    if (real < flashsizeAV)
    {
#if 0
        uint8 ret = osal_snv_read(BLE_NVID_IF_NEED_ERASE, 1, &ifNeedErase);
        if (ret == SUCCESS)
        {
            if(ifNeedErase == 1)
            {
                g_pageActiveOffset = 0;
                uint8 pg = real / HAL_FLASH_PAGE_SIZE;
                pg++;
                for (uint8 i = 0; i < pg; i++)
                {
                    HalFlashErase(g_activePg + i);
                }
                return 0;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
#else
        //P0_1 = 1; // Orange LED.
        return flashsizeAV;
#endif
    }
enoughExits:
    ELARA_LED2 = 1;
    ELARA_LED1 = 1;
    allFlashSize = (uint32)(ELARA_PAGE_END - ELARA_PAGE_BEGIN + 1) * (uint32)HAL_FLASH_PAGE_SIZE;
    if (real < allFlashSize)
    {
#if 0
        g_activePg = ELARA_PAGE_BEGIN;
        g_pageActiveOffset = 0;
        g_beginPg = g_activePg;
        g_pageBeginOffset = g_pageActiveOffset;
        osal_snv_write(BLE_NVID_BEGIN_PAGE, 1, &g_beginPg);
        osal_snv_write(BLE_NVID_BEGIN_OFFSET, 2, &g_pageBeginOffset);
        osal_snv_write(BLE_NVID_ACTIVE_PAGE, 1, &g_activePg);
        osal_snv_write(BLE_NVID_ACTIVE_OFFSET, 2, &g_pageActiveOffset);
#endif
        //osal_snv_write(BLE_NVID_MCU_BIN_SIZE, 4, &g_bin_size);
        //uint8 needErase = 1;
        //osal_snv_write(BLE_NVID_IF_NEED_ERASE, 1, &needErase);
        #if 0
        for (uint8 i = ELARA_PAGE_BEGIN; i <= ELARA_PAGE_END; i++)
        {
            P0_1 = 1; // Orange LED.
            ELARA_LED1 = 1;
            HalFlashErase(i);//Flash-page erase time: 20 ms
            ELARA_LED1 = 0;
            P0_1 = 0; // Orange LED.
        }
        return allFlashSize;
        #endif
        return 0xFFFFFFFF;
    }
    return 0;
}

int elara_flash_erase_cc2541(void)
{
    g_activePg = ELARA_PAGE_BEGIN;
    g_pageActiveOffset = 0;
    g_beginPg = g_activePg;
    g_pageBeginOffset = g_pageActiveOffset;
    osal_snv_write(BLE_NVID_BEGIN_PAGE, 1, &g_beginPg);
    osal_snv_write(BLE_NVID_BEGIN_OFFSET, 2, &g_pageBeginOffset);
    osal_snv_write(BLE_NVID_ACTIVE_PAGE, 1, &g_activePg);
    osal_snv_write(BLE_NVID_ACTIVE_OFFSET, 2, &g_pageActiveOffset);
    for (uint8 i = ELARA_PAGE_BEGIN; i <= ELARA_PAGE_END; i++)
    {
        ELARA_LED2 = 1;
        ELARA_LED1 = 1;
        HalFlashErase(i);//Flash-page erase time: 20 ms
        ELARA_LED1 = 0;
        ELARA_LED2 = 0;
    }
    return 0;
}

bool elara_flash_if_erase_finished_cc2541(void)
{
    return TRUE;
}
/*********************************************************************
 * @fn      elara_flash_read
 *
 * @brief   Read data from flash.
 *
 * @param   len - len%256 = 0
 * @param   pBuf - Data is read into this buffer.
 *
 * @return
 */
void elara_flash_read_cc2541( uint16 len, uint8 *pBuf )
{
    uint16 firstLen, lastLen;
    if (g_pageActiveOffset + len > HAL_FLASH_PAGE_SIZE)
    {
        firstLen = HAL_FLASH_PAGE_SIZE - g_pageActiveOffset;
        HalFlashRead(g_activePg, g_pageActiveOffset, pBuf, firstLen);

        g_activePg++;
        g_pageActiveOffset = 0;
        lastLen = len - firstLen;
        HalFlashRead(g_activePg, g_pageActiveOffset, pBuf + firstLen, lastLen);
        g_pageActiveOffset += lastLen;
    }
    else
    {
        HalFlashRead(g_activePg, g_pageActiveOffset, pBuf, len);
        g_pageActiveOffset += len;
        if (g_pageActiveOffset == HAL_FLASH_PAGE_SIZE)
        {
            g_pageActiveOffset = 0;
            g_activePg++;
        }
    }
}

uint8 elara_flash_test_read_cc2541(void)
{
    uint8 buf[16];
    HalFlashRead(g_activePg, g_pageActiveOffset++, buf, 1);
    return buf[0];
}

void elara_flash_test_read_init_cc2541(void)
{
    g_activePg = g_beginPg;
    g_pageActiveOffset = g_pageBeginOffset;
}

/*********************************************************************
 * @fn      elara_flash_write
 *
 * @brief   Writes 'len' bytes to the internal flash.
 *
 * @param   len - len%4 must be zero and len must < HAL_FLASH_PAGE_SIZE
 * @param   pBuf - Valid buffer space at least as big as 'len' X 4.
 *
 * @return
 */
int elara_flash_write_cc2541(uint16 len, uint8 *pBuf)
{
    uint16 firstLen, lastLen;
    uint16 addr = (g_pageActiveOffset >> 2) + ((uint16)g_activePg << 9);
    if (g_pageActiveOffset + len > HAL_FLASH_PAGE_SIZE)
    {
        firstLen = HAL_FLASH_PAGE_SIZE - g_pageActiveOffset;
        HalFlashWrite(addr, pBuf, firstLen / HAL_FLASH_WORD_SIZE);

        g_pageActiveOffset = 0;
        g_activePg++;
        addr = (g_pageActiveOffset >> 2) + ((uint16)g_activePg << 9);
        lastLen = len - firstLen;
        HalFlashWrite(addr, pBuf + firstLen, lastLen / HAL_FLASH_WORD_SIZE);
        g_pageActiveOffset += lastLen;
        g_bin_size += len;
        return 0;
    }

    HalFlashWrite(addr, pBuf, len / HAL_FLASH_WORD_SIZE);
    g_bin_size += len;
    g_pageActiveOffset += len;
    if (g_pageActiveOffset >= HAL_FLASH_PAGE_SIZE)
    {
        g_pageActiveOffset = 0;
        g_activePg++;
    }
    return 0;
}

int elara_flash_write_done_cc2541(void)
{
    //P0_1 = 1; // Orange LED.
    uint8 ret;
    osal_snv_write(BLE_NVID_BEGIN_PAGE, 1, &g_beginPg);
    osal_snv_write(BLE_NVID_BEGIN_OFFSET, 2, &g_pageBeginOffset);
    osal_snv_write(BLE_NVID_ACTIVE_PAGE, 1, &g_activePg);
    osal_snv_write(BLE_NVID_ACTIVE_OFFSET, 2, &g_pageActiveOffset);

    osal_snv_write(BLE_NVID_MCU_BIN_SIZE, 4, &g_bin_size);

    uint8 uState = 1;
    ret = osal_snv_write(BLE_NVID_U_STATE, 1, &uState);

    return ret;
}

// When lost connection or upgrade fail, we should save current active page and offset.
void elara_flash_lost_connected_cc2541(void)
{
    osal_snv_write(BLE_NVID_ACTIVE_PAGE, 1, &g_activePg);
    osal_snv_write(BLE_NVID_ACTIVE_OFFSET, 2, &g_pageActiveOffset);
}

uint32 elara_flash_any_thing_else_cc2541(void)
{
    elara_flash_write_init_cc2541();
    return 0;
}
/*********************************************************************
*********************************************************************/
