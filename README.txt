Project Name : Elara
TI协议栈版本：1.4.1
----------------------------------------------------------------------------------
下面我简述一下这个逻辑：
1、给CC2541的FFE0这个Service中的FFE1特征值发送5个字节，如下：
    uint8_t requestData[5];
    requestData[0] = size & 0xff;
    requestData[1] = (size >> 8) & 0xff;
    requestData[2] = (size >> 16) & 0xff;
    requestData[3] = (size >> 24) & 0xff;
    requestData[4] = g_PartID;
前4个字节是要升级的bin的字节大小。第5个字节是AVR 的part id，目前有如下id：
    m328p_16M_5V = 0,//Upload speed is 115200
    m328p_8M_3_3V,   //Upload speed is 57600

    m644p_16M_5V,    //Upload speed is 115200
    m644p_8M_3_3V,   //Upload speed is 57600

    m1284p_16M_5V,   //Upload speed is 115200
    m1284p_8M_3_3V,  //Upload speed is 57600

    m32u4_16M,       //Upload speed is 57600
    m128rfa1_16M,     //Upload speed is 57600

    m128rfr2,
    m64rfr2,
    m2560,
    m2561,

    m328_16M_5V,     //Upload speed is 115200
    m328_8M_3_3V,    //Upload speed is 57600
    m644_16M_5V,     //Upload speed is 115200
    m644_8M_3_3V,    //Upload speed is 57600
    m1284_16M_5V,    //Upload speed is 115200
    m1284_8M_3_3V,   //Upload speed is 57600

2、等待CC2541回Notify通知给手机。手机会收到一个4字节的Notify，这4个字节代表CC2541剩余flash大小。
如果为0，则bin太大了。
如果为0xFFFFFFFF，则说明CC2541是有能力升级该bin的，只不过空闲flash大小不足而已。此时CC2541会在3秒后重启，并擦除已使用的flash来释放空间，从而满足升级需求。

3、如果既不为0、也不为0xFFFFFFFF，则开始每20毫秒给CC2541的FFE2特征值写16字节的bin数据，直到传输完毕。
只有当手机收到FFE2的Notify时(8字节数据，前4字节代表收到的bin大小，后4个字节代表剩余flash大小），才说明CC2541真正的收到了所有bin数据。
接着手机FFE2会收到“Begin update”，提示用户正在给AVR升级；当手机收到FFE2收到“Update done”时，说明升级AVR完毕了。

附件是可以工作的CC2541的hex，可直接下载到CC2541里。
CC2541的P02接m328p、m644p、m1284p的D1脚；CC2541的P03接m328p、m644p、m1284p的D0脚；CC2541的P06接m328p、m644p、m1284p的RESET脚。
CC2541的P10接绿灯；CC2541的P11接红灯；CC2541的P13接黄灯；CC2541的P01接橙灯。

因为已使用了P02、P03作为UART0，所以外接的SPI接口的flash需要接CC2541的P14(SS)、P15(C)、P16(MO)、P17（MI）
----------------------------------------------------------------------------------
2016年3月11日，添加了波特率的设置：
* 串口控制：
AT --> return OK
AT+BAUD? --> return OK+Get:[para1]
AT+BAUD[para1] --> return OK+Set:[para1]
para1:0~4, 0=9600;1=19200;2=38400;3=57600;4=115200;  Default:4(115200)

AT+RESET --> CC2541 reset
AT+VERS? --> return current version
----------------------------------------
手机控制：
AB00 : 设置串口速率为9600
AB01 ：设置串口速率为19200
AB02 ：设置串口速率为38400
AB03 ：设置串口速率为57600
AB04 ：设置串口速率为115200
----------------------------------------
设置完毕后，均会重启，4s后串口按新设置的波特率工作。
---------------------------------------------------------------------------------

一些测试过的mac地址：
0x987BF3953A6F